/*
Student: Clinton Booze
Professor: Jill Coddington
Week 3 - Assignment - 19.13
//#################################################################################################
(Postfix Evaluation) Write a program that evaluates a postfix expression (assume it�s valid)
such as
				6 2 + 5 * 8 4 / -  =  38
The program should read a postfix expression consisting of digits and operators into a string.
Using modified versions of the stack functions implemented earlier in this chapter, the program
should scan the expression and evaluate it. The algorithm is as follows:
	1) While you have not reached the end of the string, read the expression from left to right.
		If the current character is a digit,
			Push its integer value onto the stack (the integer value of a digit character is its
				value in the computer�s character set minus the value of '0' in the
				computer�s character set).
		Otherwise, if the current character is an operator,
		Pop the two top elements of the stack into variables x and y.
		Calculate y operator x.
		Push the result of the calculation onto the stack.
	2) When you reach the end of the string, pop the top value of the stack. This is the result
	of the postfix expression.
[Note: In Step 2 above, if the operator is '/', the top of the stack is 2 and the next element in the
stack is 8, then pop 2 into x, pop 8 into y, evaluate 8 / 2 and push the result, 4, back onto the stack.
This note also applies to operator '�'.] The arithmetic operations allowed in an expression are
		+ addition
		� subtraction
		* multiplication
		/ division
		^ exponentiation
		% modulus
[Note: We assume left-to-right associativity for all operators for the purpose of this exercise.]
The stack should be maintained with stack nodes that contain an int data member and a pointer to
the next stack node. You may want to provide the following functional capabilities:
		a) function evaluatePostfixExpression that evaluates the postfix expression
		b) function calculate that evaluates the expression op1 operator op2
		c) function push that pushes a value onto the stack
		d) function pop that pops a value off the stack
		e) function isEmpty that determines if the stack is empty
		f) function printStack that prints the stack
#################################################################################################*/

#include <iostream>
#include <stack>
#include <string>
using namespace std;

int calculate(int a, int b, char oper);
void evaluatePostfixExpression(stack<int> &myStack, string &formula);
void instructions();
bool isEmpty(stack<int> myStack);
int pop(stack<int> &myStack);
void printStack(stack<int> myStack);
void push(stack<int> &myStack, int num);

int main()
{
	//declaring
	stack< int > intStack;
	string input;

	//getting info for program
	instructions();
	getline(cin, input);
	evaluatePostfixExpression(intStack, input);

	system("pause");
	return 0;
}

//doing the math
int calculate(int a, int b, char oper)
{
	switch(oper)
	{
	case '+': {
		return b + a;
		break; }
	case '-': {
		return b - a;
		break; }
	case '*': {
		return b * a;
		break; }
	case '/': {
		return b / a;
		break; }
	case '^': {
		int pwr = b;
		for(int i = 1; i < a; i++) pwr *= b;
		return pwr;
		break; }
	case '%': {
		return b % a;
		break; }
	default: {
		cerr << "\nOperator not found!\n";
		return (EXIT_FAILURE);
		break; }
	}
}

//reading in the information and running
void evaluatePostfixExpression(stack<int> &myStack, string &formula)
{
	int first, second, calc, len;
	char oper;
	len = formula.length();

	for(int i = 0; i < len; i++)
	{
		char it = formula[i];
		if (it >= '0' && it <= '9')
		{
			first = it - '0';
			push(myStack, first);
		}else if (it != ' ')
		{
			oper = it;
			first = pop(myStack);
			second = pop(myStack);
			calc = calculate(first, second, oper);
			push(myStack, calc);
		} 
	}
	cout << "Answer = ";
	printStack(myStack);
}

//displaying instructions
void instructions()
{
	cout << "Please enter numbers and operators";
	cout << "\nExample: 6 2 + 5 * 8 4 / -";
	cout << "\nValid operators: +, -, *, /, ^, %";
	cout << "\nAvoid zeros, dividing by zero ends all things!";
	cout << "\nInput: ";
}

//returning whether or not the stack is empty
bool isEmpty(stack<int> myStack)
{
	return myStack.empty();
}

//removing info from the stack
int pop(stack<int> &myStack)
{
	int num = myStack.top();
	myStack.pop();
	return num;
}

//displaying the contents of the stack
void printStack(stack<int> myStack)
{
	while(!isEmpty(myStack))
	{
		cout << myStack.top() << " ";
		myStack.pop(); //not sure if needed
	}
}

//saving info onto the stack
void push(stack<int> &myStack, int num)
{
	myStack.push(num);
}