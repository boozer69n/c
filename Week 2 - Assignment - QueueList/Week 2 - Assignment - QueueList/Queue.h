/*
Author: Clinton Booze
Professor: Jason Turner
Week 2 - Assignment - Queues
###################################################################################################
Assignment
Queues
##Implementation
Implement a templated queue list in C++. Your linked list class should implement the following
functionality:
	empty - Test whether container is empty (public member function )
	size - Return size (public member function )
	front - Access next element (public member function )
	back - Access last element (public member function )
	push - Insert element (public member function )
	pop - Remove next element (public member function )'

##Lab Report
In Word or a similar word processing program, write a Lab Report including the following sections:
   �    Introduction � in your own words, describe the structure and operations of linked lists.
   �    Programmer�s Guide  � Provide detailed documentation describing your implementation of
		the queue.  Describe the structure of the queue class (class diagrams are awesome), and
		describe the meaning and operation of each of its member variables and methods. For each
		member function,  give a usage example.  Provide a video and/or screen capture demoing
		your project.

##Submission
Submit a *.zip file containing your lab report and code project folder to the drop-box associated
with this assignment.  Name the *.zip file with your Last Name, and the week.  For example:
	DoeWeek2B.zip
###################################################################################################
*/

#ifndef QUEUE_H
#define QUEUE_H

template <class T> class Queue
{

//creating what will be int he node	
struct QueueItem {
	QueueItem *next;
	T element;
};

//private variables
private:
	QueueItem *current;
	QueueItem *last;
	int queueSize;

//public methods
public:
	Queue(void);
	virtual ~Queue(void);

	bool empty();
	int size();
	T front();
	T back();
	void push(T object);
	T pop();
	void clear();
};

//header
//#####################################################
//cpp

//constructor
template <class T> Queue<T>::Queue(void){
	this->queueSize = 0;
	this->current = 0;
	this->last = 0;
}

//returning whether the queue is empty or not
template <class T> bool Queue<T>::empty(){
	return this->queueSize == 0;
} //end empty

//returning the size of the queue
template <class T> int Queue<T>::size(){
	return this->queueSize;
} //end size

//returning the element stored within the first node
template <class T> T Queue<T>::front(){
	return this->current->element;
} //end front

//returning the element at the back of the queue
template <class T> T Queue<T>::back(){
	return this->last->element;
} //end back

//pushing a new item onto the queue
template <class T> void Queue<T>::push(T object){ //passing in templated object
	QueueItem *newElement = new QueueItem; //pointing to the memory location of the new QueueItem
	newElement->next = 0; //entering in something so it doesnt crash
	newElement->element = object; //setting the temp newElement as the passed in templated object
	
	if (this->empty()){ //if this is the first node being created then make it the current node
		this->current = newElement;
	}else{
		this->last->next = newElement; //setting the pointer location of the new node to the previous node
	}
	this->last = newElement; //making the last node as the newly created node

	this->queueSize++;
} //end push

//popping a node from the list
template <class T> T Queue<T>::pop(){
	if(this->queueSize != 0){ //checking if list is empty
		T temp = this->current->element;
		QueueItem *nextItem = this->current->next;
		delete this->current;
		this->current = nextItem;
		this->queueSize--;
		return temp;
	}else{
		return 0;
	} //end if
} //end pop

//clearing the list from memory
template <class T> void Queue<T>::clear(){
	while(this->queueSize > 0){ //while nodes still in queue
		QueueItem *temp = this->current->next;
		delete this->current;
		this->current = temp;
		this->queueSize--;
	} //end while
} //end clear

//destructor
template <class T> Queue<T>::~Queue(void){
	this->clear();
}

#endif