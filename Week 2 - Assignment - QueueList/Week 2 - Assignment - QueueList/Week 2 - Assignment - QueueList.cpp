#include <iostream>
#include "Queue.h"
#include "LinkList.h"
using namespace std;

Queue<int> myQueue;
LinkList<int> myList;
void queueTest();
void listTest();

int main(){
	queueTest();
	listTest();
}

//runs the test on the linked list queue template
void queueTest(){
	cout << "Queue Test" << endl;
	cout << "#########################" << endl;

	//filling the queue
	myQueue.push(10);
	myQueue.push(20);
	myQueue.push(30);
	myQueue.push(40);
	myQueue.push(50);

	//displaying the functions of the queue
	cout << "Back: " << myQueue.back() << endl;
	cout << "Queue Empty: " << (myQueue.empty() ? "true" : "false") << endl;
	cout << "Front: " << myQueue.front() << endl;
	cout << "Size: " << myQueue.size() << endl;
	cout << "Popping: " << myQueue.pop() << endl;

	//clearing out the queue
	while (!myQueue.empty()){
		cout << "Deleting Node: " << myQueue.pop() << endl;
	} //end while
	cout << "#########################" << endl;
	system ("pause");
} //end queueTest

//runs the test on the linked list template
void listTest(){
	cout << "\n\nLinked List Test" << endl;
	cout << "#########################" << endl;

	//filling the list
	myList.nodeInsert(10);
	myList.nodeInsert(20);
	myList.nodeInsert(30);
	myList.nodeInsert(40);
	myList.nodeInsert(50);

	//displaying the functions of the list
	cout << "Find: " << myList.nodeFind(30) << endl;
	cout << "Minimum: " << myList.nodeMin() << endl;
	cout << "Maximum: " << myList.nodeMax() << endl;
	cout << "Size of List: " << myList.nodeCount() << endl;
	myList.nodeDelete(myList.nodeFind(30));
	cout << "Size after deleting element 30: " << myList.nodeCount() << endl;
	cout << "Clearing list" << endl;
	myList.listClear();

	cout << "#########################" << endl;
	system ("pause");
} //end listTest