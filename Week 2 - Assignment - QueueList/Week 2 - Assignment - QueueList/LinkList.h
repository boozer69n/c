#ifndef LINKLIST_H
#define LINKLIST_H

template <class T> class LinkList
{

//structure containing node info
public:
struct LinkListItem {
	LinkListItem *next;
	LinkListItem *previous;
	T element;
};

//private variables
private:
	LinkListItem *first;
	LinkListItem *last;
	int linklistSize;
	T minElement;
	T maxElement;

//public methods
public:
	LinkList(void);
	virtual ~LinkList(void);

	LinkListItem *nodeFind(T element);
	void nodeInsert(T object);
	T nodeDelete(LinkListItem *object);
	T nodeMin();
	T nodeMax();
	int nodeCount();
	void listClear();
};

//header
//#####################################################
//cpp

//constructor
template <class T> LinkList<T>::LinkList(void){
	this->linklistSize = 0;
	this->first = 0;
	this->last = 0;
}

//insert a new node into the linked list
template <class T> void LinkList<T>::nodeInsert(T object){
	LinkListItem *newElement = new LinkListItem;
	newElement->element = object;
	newElement->next = 0;
	newElement->previous = this->last;

	//checking for max element
	if (object > this->maxElement){
		this->maxElement = object;
	} //end if

	//checking for min element
	if (object < this->minElement){
		this->minElement = object;
	} //end if

	//checking if list is empty
	if (this->linklistSize == 0){
		this->first = newElement;
		this->minElement = object;
		this->maxElement = object;
	}else{
		this->last->next = newElement;
	}//end if

	//updating variables
	this->last = newElement;
	this->linklistSize++;
} //end nodeInsert

//deleting a node from the list
template <class T> T LinkList<T>::nodeDelete(LinkListItem *object){
	//saving information from soon to be deleted node
	T element = object->element;
	LinkListItem *before = object->previous;
	LinkListItem *after = object->next;

	//modifying adjacent nodes
	before->next = after;
	after->previous = before;

	//deleting node
	delete object;

	//updating list count and returning deleted node info
	this->linklistSize--;
	return element;
} //end nodeDelete

//search the list to find a specific node based off of element
//have to fully qualify LinkListItem (LinkList<T>) using typename
template <class T> typename LinkList<T>::LinkListItem * LinkList<T>::nodeFind(T element){
	//creating temp variables
	int listSize = this->linklistSize;
	LinkListItem *temp = new LinkListItem;
	temp = this->first;

	//searching the list
	while (listSize > 0){
		if (temp->element == element){
			return temp;
		} //end if
		temp = temp->next;
		listSize--;
	} //end while
	return 0;
} //end nodeFind

//returns the minimum value element
template <class T> T LinkList<T>::nodeMin(){
	return this->minElement;
} //end nodeMin

//returns the max value element
template <class T> T LinkList<T>::nodeMax(){
	return this->maxElement;
} //end nodeMax

//clearing the list freeing up the memory
template <class T> void LinkList<T>::listClear(){
	while(this->linklistSize > 0){
		LinkListItem *temp = this->first->next;
		delete this->first;
		this->first = temp;
		this->linklistSize--;
	} //end while
} //end listClear

//returning the size of the list
template <class T> int LinkList<T>::nodeCount(){
	return this->linklistSize;
} //end nodeCount

//destructor
template <class T> LinkList<T>::~LinkList(void){
	this->listClear();
}

#endif