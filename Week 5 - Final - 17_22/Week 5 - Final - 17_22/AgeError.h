#ifndef AGEERROR_H //for compatibility
#define AGEERROR_H //for compatibility

#include <stdexcept> //for errors
#include <string> //adding strings
using std::string; //to reduce memory
using std::runtime_error; //to reduce memory

class AgeError : public runtime_error //constructor subclass runtime_error
{
public:
	//receive error send up to runtime_error, then exception
	explicit AgeError(const string& what) : runtime_error(what) {}
	//receive error send up to runtime_error, then exception
	explicit AgeError(const char* what) : runtime_error(what) {}
};

#endif //AGEERROR_H