//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Final - 17.22
//#################################################################################################
#include <iostream> //for displaying
#include <stdexcept> //for errors
#include <string> //using string
#include "Age.h" //adding header
#include "FatherSonAge.h" //adding header
using namespace std;
//#################################################################################################
/*
(Catching Derived-Class Exceptions) Use inheritance to create various derived classes of
RUNTIME_ERROR. Then show that a CATCH handler specifying the base class can CATCH
derived-class exceptions.
*/
//#################################################################################################

void ageCheck(); //defining function
void fsCheck(); //defining function

int main() //begin main
{
	string input;
	do
	{
		//selection ui loop
		cout << "\nPlease choose one" << endl <<
				"1 - Valid age check" << endl <<
				"2 - Father/Son age check/comparison" << endl <<
				"3 - Exit" << endl <<
				"Selection: ";
		cin >> input;

		if (input == "1")
		{
			ageCheck();
		}
		if (input == "2")
		{
			fsCheck();
		}
	} while (input != "3"); //end if 3 is entered

} //end main

//#################################################################################################

void ageCheck() //check for valid age function
{
	Age age; //age class

	//get an age to check
	string ageInput;
	cout << "Please enter an age to check: ";
	cin >> ageInput;

	try //begin error check
	{
	age.setAge (ageInput); //sending through checks
	}

	catch (invalid_argument e) //error catch if an integer is not entered
	{
		cout << "---------------------------------" << endl <<
				"Error Message:" << endl <<
			    "Must be an integer (whole number)" << endl <<
				"---------------------------------" << endl;
	}
	catch (runtime_error e) //error catch for other errors checked
	{
		cout  << "---------------------------------" << endl <<
			     "Error Message:" << endl <<			
				 e.what() << endl <<
				 "---------------------------------" << endl;
	}
} //end ageCheck

//#################################################################################################

void fsCheck() //check father/son ages and compare
{
	FatherSonAge fsAge; //fathersonage class

	string fAge; //fathers age for later input
	string sAge; //sons age for later input

	//obtaining ages from user
	cout << "\nPlease enter fathers age: ";
	cin >> fAge;
	cout << "\nPlease enter sons age: ";
	cin >> sAge;

	try //begin error check
	{
		fsAge.setFatherSonAge(fAge, sAge); //sending through checks
	}
	catch (invalid_argument e) //catch for invalid format
	{
		cout << "---------------------------------" << endl <<
				"Error Message:" << endl <<
			    "Must be an integer (whole number)" << endl <<
				"---------------------------------" << endl;
	}
	catch (runtime_error e) //catch for other errors
	{
		cout  << "---------------------------------" << endl <<
			     "Error Message:" << endl <<			
				 e.what() << endl <<
				 "---------------------------------" << endl;
	}
} //end fsCheck

//#################################################################################################