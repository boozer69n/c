#include <string> //using string
#include "Age.h" //adding header
#include "NegativeAge.h" //adding header
#include "ZeroError.h" //adding header
using std::string; //added to reduce memory

Age::Age() //constructor
{
	age = 0; //defaulting age to 0
}

void Age::setAge (const string &age) //overloaded to accept string
{
	setAge(stoi(age)); //format check
}

void Age::setAge (int age) //overloaded to accept int
{
	check (age); //running checks
	this->age = age; //if good sets age
}

void Age::check (int number) //number checking
{
	if (number < 0) //checking if negative
	{
		//thrown error if negative
		throw NegativeAge("Must be positive numbers");
	}

	if (number == 0) //checking if 0
	{
		//thrown error if 0
		throw ZeroError("Age must be above 0");
	}
}

int Age::getAge() const //returning age
{
	return this->age; //returning age
}

Age::~Age()
{
}
