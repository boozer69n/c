#ifndef FATHERSONAGE_H //for compatibility
#define FATHERSONAGE_H //for compatibility

#include <string> //adding string
#include "age.h" //adding header
using std::string; //to reduce memory

class FatherSonAge : public Age //constructor subclass age
{
public:
	FatherSonAge();
	virtual ~FatherSonAge(); //destructor

	void setFatherSonAge (const string &fAge, const string &sAge); //overloaded to receive string
	void setFatherSonAge (int fAge, int sAge); //overloaded to receive int

	int getFatherAge () const; //to return fathers age
	int getSonAge () const; //to return sons age

private:
	int sonAge; //variable for sons age
};

#endif //FATHERSONAGE_H