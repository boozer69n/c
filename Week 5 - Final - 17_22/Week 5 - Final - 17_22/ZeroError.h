#ifndef ZEROERROR_H //for compatibility
#define ZEROERROR_H //for compatibility

#include <string> //for strings
#include "AgeError.h" //adding header
using std::string; //added to reduce memory

class ZeroError : public AgeError //constructor sub-classing ageerror
{
public:
	explicit ZeroError(const string& what) : AgeError(what) {} //receive error send up to ageerror
	explicit ZeroError(const char* what) : AgeError(what) {} //receive error send up to ageerror
};

#endif //ZEROERROR_H