#ifndef NEGATIVEAGE_H //for compatibility
#define NEGATIVEAGE_H //for compatibility

#include <string> //adding string
#include "AgeError.h" //adding header
using std::string; //to reduce memory

class NegativeAge : public AgeError //constructor subclass ageerror
{
public:
	explicit NegativeAge(const string& what) : AgeError(what) {} //receive error send up to ageerror
	explicit NegativeAge(const char* what) : AgeError(what) {} //receive error send up to ageerror
};

#endif //NEGATIVEAGE_H