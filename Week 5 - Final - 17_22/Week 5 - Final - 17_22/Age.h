#ifndef AGE_H //for compatibility
#define AGE_H //for compatibility

#include <string> //using string
using std::string;

class Age //constructor
{
public:
	Age();
	virtual ~Age(); //destructor

	void setAge (const string &age); //overloaded to accept string
	void setAge (int age); //overloaded to accept int
	int getAge () const; //returning age

protected: //protected so only subclasses have access
	void check (int number); //running check on number

private:
	int age; //age variable
};

#endif //AGE_H