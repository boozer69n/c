#include "Age.h" //adding header
#include "FatherSonAge.h" //adding header
#include "FatherSonAgeError.h" //adding header

FatherSonAge::FatherSonAge(void) : Age() //constructor subclass age
{
	sonAge = 0; //defaulting sons age to 0
}

//overloaded to accept string
void FatherSonAge::setFatherSonAge (const string &fAge, const string &sAge)
{
	setFatherSonAge(stoi(fAge), stoi(sAge)); //format check
}

//overloaded to accept int
void FatherSonAge::setFatherSonAge (int fAge, int sAge)
{
	Age::check(fAge); //number checks
	Age::check(sAge); //number checks
	if (fAge <= sAge) //comparison check
	{
		//thrown error if father younger than son
		throw FatherSonAgeError("Father must be older than son");
	}
	
	Age::setAge (fAge); //setting fathers age if no errors
	this->sonAge = sAge; //setting sons age if no errors
}

int FatherSonAge::getFatherAge () const
{
	return Age::getAge (); //returning fathers age
}

int FatherSonAge::getSonAge () const
{
	return this->sonAge; //returning sons age
}

FatherSonAge::~FatherSonAge(void) //deconstructor
{
}
