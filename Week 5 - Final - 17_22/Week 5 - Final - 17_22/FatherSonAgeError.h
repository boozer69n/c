#ifndef FATHERSONAGEERROR_H //for compatibility
#define FATHERSONAGEERROR_H //for compatibility

#include <string> //adding string
#include "AgeError.h" //adding header
using std::string; //to reduce memory

class FatherSonAgeError : public AgeError //constructor subclass ageerror
{
public:
	explicit FatherSonAgeError(const string& what) : AgeError(what) {} //receive error send up to ageerror
	explicit FatherSonAgeError(const char* what) : AgeError(what) {} //receive error send up to ageerror
};

#endif //FATHERSONAGEERROR_H