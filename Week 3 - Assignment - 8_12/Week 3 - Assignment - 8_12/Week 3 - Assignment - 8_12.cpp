//Student: Clinton Booze
//Professor: Victoria Heil
//Week 3 - Assignment - 8_12
//Tortoise and the Hare
//############################################################################################################ 
#include <iostream> //for displaying
#include <ctime> //added for random numbers
#include <windows.h> //added for sleep
using namespace std;
//############################################################################################################ 

void moveTortoise(char *tortusPos) //creating function to move the tortoise
{
	int roll = rand() % 10 + 1; //rolling a random number between 1-10

	if (roll <= 5) //if the roll is 1-5
		*tortusPos += 3; //move the tortoise 3 spaced ahead
	else if (roll <= 7) // if the roll is 6-7
		*tortusPos -= 6; //move the tortoise 6 spaced back
	else //else
		*tortusPos += 1; //move the tortoise 1 spaced ahead

	if (*tortusPos < 1) //if the tortoise moved back before the starting line
		*tortusPos = 1; //it moves him to the starting line
}

void moveHare(char *hairPos) //creating function to move the hare
{
	int roll = rand() % 10 + 1; //rolling a random number between 1-10

	if (roll <= 2) //if a 1-2 is rolled
		*hairPos += 0; //don't move
	else if (roll <= 4) //if a 3-4 is rolled
		*hairPos += 9; //move ahead 9
	else if (roll <= 5) //if a 5 is rolled
		*hairPos -= 12; //move back 12
	else if (roll <= 8) //if a 6-8 is rolled
		*hairPos += 1; //move ahead 1
	else //else
		*hairPos -= 2; //move back 2

		if (*hairPos < 1) //if the hare moved back behind the starting line
			*hairPos = 1; //it moves him to the starting line
}

void trackDisplay(char tortusPos, char hairPos) //creates a function to display the track
{
	for (int i = 0 ; i < 70 ; i++) //for loop to display the 70 spaces of the track
	{
		if (tortusPos == hairPos && hairPos == i) //if they are on the same spot
			cout << "OUCH!!!"; //display ouch
		else if (hairPos == i) //if this loop matches the hares spot
			cout << "H"; //displays H
		else if (tortusPos == i) //if this loop matches the tortoises spot
			cout << "T"; //display T
		else //else
			cout << " "; //makes a dash
	
	}
	cout << endl; //space for clarity
}

int main() //initialize program
{
    srand( time(0) ); //random number seed
    cout << "BANG!!!!!\nAND THEY'RE OFF!!!!!\n"; //so the animals know when to start...lol
	
	char tortusPlace = 1; //start location
	char hairPlace = 1; //start location

	while (tortusPlace < 70 && hairPlace < 70) //loop to keep going until one wins
	{
		moveTortoise(&tortusPlace); //running the moveTortoise function which moves the tortoise
		moveHare(&hairPlace); //running the moveHare function which moves the hare
		trackDisplay(tortusPlace, hairPlace); //displaying the track which also displays the animals position
		Sleep(1000); //waiting 1 second before next run

	}

	if (tortusPlace >= 70 && hairPlace >=70) //checking if there was a tie
	{
		cout << "It's a tie." << endl; //displaying if there is a tie
	}
	else if (tortusPlace >= 70) //checking if the tortoise wins
	{
		cout << "TORTOISE WINS!!! YAY!!!" << endl; //displaying that the tortoise wins
	}
	else // else
	{	
		cout << "Hare wins. Yuch." << endl; //displaying that the hare wins
	}

	system("pause"); //keeping window up
}