#include <iostream> //display
#include <stdexcept> //exception argument
#include "PieceWorker.h" //class definition
//used to minimize memory usage
using std::string; //string
using std::cout; //cout
using std::invalid_argument; //argument

PieceWorker::PieceWorker( const string &first,
						  const string &last,
						  const string &ssn,
						  int pieces,
						  double wage )
						  : Employee( first, last, ssn ) //constructor

//to catch any errors by entering negative numbers and setting wage/pieces
{
	try //in case there is an error
	{
		setPieces( pieces ); //if no error continue
	}
	catch (invalid_argument e) //if error
	{
		cout << e.what(); //display error returned
		this->pieces = 0; //default number to 0
	}

	try //in-case there is an error
	{
		setWage( wage ); //if no error continue
	}
	catch (invalid_argument e) //if error
	{
		cout << e.what(); //display error returned
		this->wage = 0; //default number to 0
	}
}

void PieceWorker::setPieces (int pieces) //setting pieces variable
{
	if (pieces >= 0) //checking for negative number
	{
		this->pieces = pieces; //if ok
	}
	else
		throw invalid_argument ("Pieces set below 0, defaulted to 0"); //if not return error
}

int PieceWorker::getPieces () const //getting pieces
{
	return this->pieces; //returning pieces
}

void PieceWorker::setWage (double wage) //setting wage
{
	if (wage >= 0) //checking for negative number
	{
		this->wage = wage; //if ok
	}
	else
		throw invalid_argument ("Wage set below 0, defaulted to 0"); //if not return error
}

double PieceWorker::getWage () const //getting wage
{
	return this->wage; //returning wage
}

double PieceWorker::earnings() const //calculating earnings
{
	return ((this->pieces) * (this->wage)); //returning earnings
}
void PieceWorker::print() const //printing employee info/pay
{
   cout << "pieces employee: ";
   Employee::print(); // code reuse
   cout << "\npay per piece: " << this->wage << "; pieces sold: " << this->pieces;
}

PieceWorker::~PieceWorker(void) //destructor
{
}
