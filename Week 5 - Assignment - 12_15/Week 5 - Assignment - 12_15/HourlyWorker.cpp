#include <iostream> //display
#include <stdexcept> //exception argument
#include "HourlyWorker.h" //class definition
//used to minimize memory usage
using std::string; //string
using std::cout; //cout
using std::invalid_argument; //argument

HourlyWorker::HourlyWorker( const string &first,
						   const string &last,
						   const string &ssn,
						   double hours,
						   double wage )
						   : Employee( first, last, ssn )  //constructor

//to catch any errors by entering negative numbers and setting wage/hours
{
	try //in case there is an error
	{
		setHour( hours ); //if no error continue
	}
	catch (invalid_argument e) //if error
	{
		cout << e.what(); //display error returned
		this->hours = 0; //default number to 0
	}

	try //in case there is an error
	{
		setWage( wage ); //if no error continue
	}
	catch (invalid_argument e) //if error
	{
		cout << e.what(); //display error returned
		this->wage = 0; //default number to 0
	}
}

void HourlyWorker::setHour (double hours) //add hours in the week
{
	if (hours >= 0) //checking for negatives
	{
		this->hours = hours; //if good
	}
	else if (hours > 168) //if hours are more than hours in a week
	{
		throw invalid_argument("Hours more than hours in a week,  defaulted to 0"); //return error
	}
	else
	{
		throw invalid_argument("Hours set below 0, defaulted to 0"); //if negative return error
	}	
}

double HourlyWorker::getHour () const //getting hours
{
	return this->hours; //returning hours
}

void HourlyWorker::setWage (double wage) //setting wage
{
	if (wage >= 0) //checking for negative numbers
	{
		this->wage = wage; //setting wage
	}
	else
	{
		throw invalid_argument("Wage set below 0, defaulted to 0"); //if negative error returned
	}
}

double HourlyWorker::getWage () const //getting wage
{
	return this->wage; //wage returned
}

double HourlyWorker::earnings() const //getting earnings
{
	double normalHours = this->hours > 40.0 ? 40.0 : this->hours; //setting hours for <=40
	double otHours = this->hours - normalHours; //setting OT hours
	return ((normalHours * this->wage) + (otHours * (this->wage * 1.5))); //returning earnings
}

void HourlyWorker::print() const //displaying employee info/pay
{
   cout << "hourly employee: ";
   Employee::print(); // code reuse
   cout << "\nhourly pay: " << this->wage << "; hours worked: " << this->hours;
}

HourlyWorker::~HourlyWorker(void) //destructor
{
}
