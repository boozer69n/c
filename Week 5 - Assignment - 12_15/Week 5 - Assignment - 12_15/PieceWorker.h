#ifndef PIECEWORKER_H
#define PIECEWORKER_H

#include <string> //string class
#include "Employee.h" //employee definition
using std::string; //using string

class PieceWorker : public Employee //pieceworker class subclass employee
{
public:
	PieceWorker(const string &first,
				const string &last,
				const string &ssn,
				int pieces = 0.0,
				double wage = 0.0); //constructor
	virtual ~PieceWorker(void); //destructor

	void setPieces (int); //setting number of pieces
	int getPieces () const; //getting variable pieces

	void setWage (double); //setting wage
	double getWage () const; //getting variable wage

   virtual double earnings() const; //pay check
   virtual void print() const; //displaying employee info and pay

private:
	double wage; //amount per piece paid
	int pieces; //number of pieces made

};

#endif // PIECEWORKER_H