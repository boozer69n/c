#ifndef HOURLYWORKER_H
#define HOURLYWORKER_H

#include <string> //string class
#include "Employee.h" //employee definition
using std::string; //using string

class HourlyWorker : public Employee //hourly worker subclass employee
{
public:
	HourlyWorker(const string &first,
				const string &last,
				const string &ssn,
				double hours = 0.0,
				double wage = 0.0); //constructor
	virtual ~HourlyWorker(); //deconstructor

	void setHour (double); //setting hours
	double getHour () const; //getting variable hours

	void setWage (double); //setting wage
	double getWage () const; //returning variable wage
	
	virtual double earnings() const; //setting earnings
	virtual void print() const; //displaying employee info/pay
	
private:
	double hours; //number of hours worked
	double wage; //hourly wage

};

#endif // HOURLYWORKER_H