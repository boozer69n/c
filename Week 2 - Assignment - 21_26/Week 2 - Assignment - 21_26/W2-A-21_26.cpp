/*
Student: Clinton Booze
Professor: Jill Coddington
Week 2 - Assignment - 21.26
//#################################################################################################
(Cooking with Healthier Ingredients)
Obesity in the United States is increasing at an alarming
rate. Check the map from the Centers for Disease Control and Prevention (CDC) at
www.cdc.gov/nccdphp/dnpa/Obesity/trend/maps/index.htm, which shows obesity trends in the
United States over the last 20 years. As obesity increases, so do occurrences of related problems (e.g.,
heart disease, high blood pressure, high cholesterol, type 2 diabetes). Write a program that helps
users choose healthier ingredients when cooking, and helps those allergic to certain foods (e.g., nuts,
gluten) find substitutes. The program should read a recipe from the user and suggest healthier replacements
for some of the ingredients. For simplicity, your program should assume the recipe has
no abbreviations for measures such as teaspoons, cups, and tablespoons, and uses numerical digits
for quantities (e.g., 1 egg, 2 cups) rather than spelling them out (one egg, two cups). Some common
substitutions are shown in Fig. 21.14. Your program should display a warning such as, �Always consult
your physician before making significant changes to your diet.�

Ingredient					Substitution
1 cup sour cream			1 cup yogurt
1 cup milk					1/2 cup evaporated milk and 1/2 cup water
1 teaspoon lemon juice		1/2 teaspoon vinegar
1 cup sugar					1/2 cup honey, 1 cup molasses
							or 1/4 cup agave nectar
1 cup butter				1 cup margarine or yogurt
1 cup flour					1 cup rye or rice flour
1 cup mayonnaise			1 cup cottage cheese
							or 1/8 cup mayonnaise and 7/8 cup yogurt
1 egg						2 tablespoons cornstarch, arrowroot flour
							or potato starch or 2 egg whites
							or 1/2 of a large banana (mashed)
1 cup milk					1 cup soy milk
1/4 cup oil					1/4 cup applesauce
white bread					whole-grain bread
Fig. 21.14 | Common ingredient substitutions.

Your program should take into consideration that replacements are not always one-for-one.
For example, if a cake recipe calls for three eggs, it might reasonably use six egg whites instead.
Conversion data for measurements and substitutes can be obtained at websites such as:

chinesefood.about.com/od/recipeconversionfaqs/f/usmetricrecipes.htm
www.pioneerthinking.com/eggsub.html
www.gourmetsleuth.com/conversions.htm

Your program should consider the user�s health concerns, such as high cholesterol, high blood pressure,
weight loss, gluten allergy, and so on. For high cholesterol, the program should suggest substitutes
for eggs and dairy products; if the user wishes to lose weight, low-calorie substitutes for
ingredients such as sugar should be suggested.
//#################################################################################################
*/

#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include "Substitutions.h"
using namespace std;

//functions
double convert (string unit);
string fullBenefit (string benefit);
void getIngredient(istringstream &stream, double &amount, string &unit, string &ingredient);
void itemAdd(fstream &foodReplace, map<string, Substitutions> &subs);
void mapList(fstream &foodReplace, map<string, Substitutions> &subs);
void mapList(map<string, Substitutions> &subs, string newLine) ;
void showList (map<string, Substitutions> &subs);
void subSearch(string item, map<string, Substitutions> &subs);

//main
int main()
{
	string uiSel;
	string chkIngredient;

	//creates/opens the file
	fstream foodReplace;
	
	map<string, Substitutions> subs;
	mapList(foodReplace, subs);

	//ui
	bool uiExit = false;
	while(!uiExit)
	{
		cout << "\nFood Substitutions" 
			 << "\nPlease Select One"
			 << "\n1 - See List of Current Ingredients"
			 << "\n2 - Search for an ingredients substitute"
			 << "\n3 - Add an Item to the list"
			 << "\n0 - Exit"
			 << "\nSelection: ";

		cin >> uiSel;
		while(stoi(uiSel) > 3)
		{
			cout << "\nInvalid";
			cout << "\nSelection: ";
			cin >> uiSel;
		}
	
		if(uiSel == "1")
		{
			showList(subs);
		}
		if(uiSel == "2")
		{ 
			cout << "\nIngredient: ";
			cin.ignore();
			getline(cin, chkIngredient); 
			subSearch(chkIngredient, subs);
		}
		if(uiSel == "3")
		{
			itemAdd(foodReplace, subs);
		}
		if(uiSel == "0")
		{
			uiExit = true;
		}
		cin.ignore();
	}
	foodReplace.close();
}

//searching for item and its replacement
void subSearch(string item, map<string, Substitutions> &subs)
{
	cout.precision(3);
	//convert string to lower case
	transform(item.begin(), item.end(), item.begin(), ::tolower);

	//declaring
	double oldC;
	double newC;
	Substitutions ingRep = subs[item];

	//warning
	cout << "\n############################################################################";
	cout << "\nAlways consult your physician before making significant changes to your diet";
	cout << "\n############################################################################";
	
	//displaying original item and amount
	cout << "\n\n-=Original Ingredient=-" << endl;
	cout << ingRep.getOldAmount() << " - " 
		 << ingRep.getOldMeasurement() << " - " 
		 << ingRep.getOldItem() << endl;

	//conversion display
	oldC = convert(ingRep.getOldMeasurement()) * ingRep.getOldAmount();
	cout << oldC << " (mL)" << endl;

	//displaying replacement item and amount
	cout << "\n-=Replacement Ingredient=-" << endl;
	cout << ingRep.getNewAmount() << " - " 
		 << ingRep.getNewMeasurement() << " - " 
		 << ingRep.getNewItem() << endl;

	//conversion display
	newC = convert(ingRep.getNewMeasurement()) * ingRep.getNewAmount();
	cout << newC << " (mL)" << endl;

	//displaying the benefit
	cout << "\n-=Benefit=-" << endl;
	cout << fullBenefit(ingRep.getBenefit()) << endl;
}

//function to add an item to the txt file
void itemAdd(fstream &foodReplace, map<string, Substitutions> &subs)
{
	//declaring variables
	double oAmnt;
	double nAmnt;
	string oUnit;
	string nUnit;
	string oItem;
	string nItem;
	int ben;
	string line = "";
	string benString;

	//opening file for appending
	foodReplace.open("foodReplace.txt", ios::app);
	//in case the file cant be opened/modified
	if(!foodReplace)
	{
		cerr << "File could not be opened" << endl;
		exit (EXIT_FAILURE);
	}

	//getting info for original ingredient
	cout << "\n-=Original Ingredient=-";
	cout << "\nExample: 1.25 (no fractions)";
	cout << "\nAmount: ";
	cin.clear();
	while(!(cin >> oAmnt))
	{
		cout << "\nInvalid\nAmount: ";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout << "\nAcceptable Measurements";
	cout << "\n(teaspoon, tablespoon, cup, ounce, gallon, pint)";
	cout << "\nMeasurement: ";
	cin >> oUnit;
	cout << "\nIngredient: ";
	cin.ignore();
	getline(cin, oItem);

	//getting info for replacement ingredient
	cout << "\n\n-=Replacement=-";
	cout << "\nExample: 1.25 (no fractions)";
	cout << "\nAmount: ";
	cin.clear();
	while(!(cin >> nAmnt))
	{
		cout << "\nInvalid\nAmount: ";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
	cout << "\nAcceptable Measurements";
	cout << "\n(teaspoon, tablespoon, cup, ounce, gallon, pint)";
	cout << "\nMeasurement: ";
	cin >> nUnit;
	cout << "\nIngredient: ";
	cin.ignore();
	getline(cin, nItem);

	bool benExit = false;
	while(!benExit)
	{
		//options for the benefit
		cout << "\n\nPlease choose a benefit";
		cout << "\n1 - Gluten Free";
		cout << "\n2 - Reduced Calorie";
		cout << "\n3 - Reduced Fat";
		cout << "\n4 - Diabetic Friendly";
		cout << "\nSelection: ";
		while(!(cin >> ben))
		{
			cout << "\nInvalid\nAmount: ";
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}

		//saving the benefit
		if(ben == 1) { benString = "gluten"; benExit = true;}
		if(ben == 2) { benString = "calorie"; benExit = true; }
		if(ben == 3) { benString = "fat"; benExit = true; }
		if(ben == 4) { benString = "diabetic"; benExit = true; }
	}

	//creating string in the proper format for txt file
	line += to_string(oAmnt) + " "
		 + oUnit + " " 
		 + oItem + " | " 
		 + to_string(nAmnt) + " " 
		 + nUnit + " " 
		 + nItem + " | " 
		 + benString + "\n";

	//convert string to lower case
	transform(line.begin(), line.end(), line.begin(), ::tolower);

	//inputting string
	foodReplace << line;

	//adding new item to the map
	mapList(subs, line);

	//closing file
	foodReplace.close();
}

//pulling in the info from the file and mapping it
void mapList(fstream &foodReplace, map<string, Substitutions> &subs) 
{
	//opens the file
	foodReplace.open("foodReplace.txt", ios::in);
	if(!foodReplace)
	{
		cerr << "File could not be opened" << endl;
		exit (EXIT_FAILURE);
	}

	string line;
	while (getline (foodReplace, line)) //gets the line and saves it to line
	{
		Substitutions item;
		istringstream readLine(line); //reads it into readLine

		//declaring variables
		double amount;
		string unit;
		string ingredient;
		string benefit;

		//gets old ingredient and saves in object
		getIngredient(readLine, amount, unit, ingredient);
		item.setOldAmount(amount);
		item.setOldMeasurement(unit);
		item.setOldItem(ingredient);

		//gets new ingredient and saves to object
		getIngredient(readLine, amount, unit, ingredient);
		item.setNewAmount(amount);
		item.setNewMeasurement(unit);
		item.setNewItem(ingredient);

		//reads in last piece and saves in object
		readLine >> benefit;
		item.setBenefit(benefit);

		//inserts object into map
		subs.insert(pair<string, Substitutions>(item.getOldItem(), item));
	}
	//closes file
	foodReplace.close();
}

//overloaded to accept a string for the adding ingredient function
void mapList(map<string, Substitutions> &subs, string newLine) 
{
	Substitutions item;
	istringstream readLine(newLine); //reads it into readLine

	//declaring variables
	double amount;
	string unit;
	string ingredient;
	string benefit;

	//gets old ingredient and saves in object
	getIngredient(readLine, amount, unit, ingredient);
	item.setOldAmount(amount);
	item.setOldMeasurement(unit);
	item.setOldItem(ingredient);

	//gets new ingredient and saves to object
	getIngredient(readLine, amount, unit, ingredient);
	item.setNewAmount(amount);
	item.setNewMeasurement(unit);
	item.setNewItem(ingredient);

	//reads in last piece and saves in object
	readLine >> benefit;
	item.setBenefit(benefit);

	//inserts object into map
	subs.insert(pair<string, Substitutions>(item.getOldItem(), item));
}

//function to extract amount-units-ingredient from the stream pulling from the file
void getIngredient(istringstream &stream, double &amount, string &unit, string &ingredient)
{
	//resetting variables
	amount = 0;
	unit = "";
	ingredient = "";
	string temp;

	//setting variables
	stream >> amount;
	stream >> unit;
	stream >> temp;

	//read until delimiter is hit
	while (temp != "|")
	{
		ingredient += temp + " ";
		stream >> temp;
	}

	//removes the space at the end of the ingredient
	ingredient = ingredient.substr(0, ingredient.length() - 1);
}

//function to display the list of ingredients
void showList (map<string, Substitutions> &subs)
{
	cout << "\nCurrent List of Ingredients" << endl;
	map<string, Substitutions>::iterator it;
	for(it = subs.begin(); it != subs.end(); it++)
	{
		string key = it->first; //key=what object is mapped to
		Substitutions sub = it->second; //sub=Substitution Object
		cout << key << endl;
	}
	cout << '\n' << endl;
}

//function to do conversions
double convert (string unit)
{
	//default value
	double conv = 1;

	//setting conversion amount to mL
	if(unit == "teaspoon")
		conv = 5;
	if(unit == "tablespoon")
		conv = 15;
	if(unit == "ounce")
		conv = 30;
	if(unit == "cup")
		conv = 240;
	if(unit == "gallon")
		conv = 3800;
	if(unit == "pint")
		conv = 500;
	return conv;
}

//returning the benefit
string fullBenefit (string benefit)
{
	//returning lengthened format of benefit
	string ben;
	if (benefit == "gluten") { ben = "gluten free"; return ben;}
	if (benefit == "gluten") { ben = "reduced calorie"; return ben; }
	if (benefit == "gluten") { ben = "reduced fat"; return ben; }
	if (benefit == "gluten") { ben = "diabetic friendly"; return ben; }
	return benefit;
}
