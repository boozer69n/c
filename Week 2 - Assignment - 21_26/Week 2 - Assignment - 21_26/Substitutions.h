#ifndef SUBSTITUTIONS_H
#define SUBSTITUTIONS_H
#include <string>
using namespace std;

class Substitutions
{
public:
	Substitutions(void);
	virtual ~Substitutions(void);

	//getters and setters
	double getOldAmount();
	void setOldAmount(double);
	double getNewAmount();
	void setNewAmount(double);
	string getOldMeasurement();
	void setOldMeasurement(string);
	string getNewMeasurement();
	void setNewMeasurement(string);
	string getOldItem();
	void setOldItem(string);
	string getNewItem();
	void setNewItem(string);
	string getBenefit();
	void setBenefit(string);

private:
	//saving each info for ingredients and replacements
	double oldAmount;
	double newAmount;
	string oldMeasurement;
	string newMeasurement;
	string oldItem;
	string newItem;
	string benefit;
};

#endif