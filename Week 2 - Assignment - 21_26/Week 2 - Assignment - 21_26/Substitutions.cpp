#include "Substitutions.h"
#include <string>
using namespace std;

//getters and setters, and do i ever miss the ability eclipse has with making these for me
Substitutions::Substitutions(void)
{
}

double Substitutions::getOldAmount()
{
	return this->oldAmount;
}

void Substitutions::setOldAmount(double amount)
{
	this->oldAmount = amount;
}

double Substitutions::getNewAmount()
{
	return this->newAmount;
}

void Substitutions::setNewAmount(double amount)
{
	this->newAmount = amount;
}

string Substitutions::getOldMeasurement()
{
	return this->oldMeasurement;
}

void Substitutions::setOldMeasurement(string unit)
{
	this->oldMeasurement = unit;
}

string Substitutions::getNewMeasurement()
{
	return this->newMeasurement;
}

void Substitutions::setNewMeasurement(string unit)
{
	this->newMeasurement = unit;
}

string Substitutions::getOldItem()
{
	return this->oldItem;
}

void Substitutions::setOldItem(string item)
{
	this->oldItem = item;
}

string Substitutions::getNewItem()
{
	return this->newItem;
}

void Substitutions::setNewItem(string item)
{
	this->newItem = item;
}

string Substitutions::getBenefit()
{
	return this->benefit;
}

void Substitutions::setBenefit(string benefit)
{
	this->benefit = benefit;
}

Substitutions::~Substitutions(void)
{
}