//Student: Clinton Booze
//Professor: Victoria Heil
//Week 2 - Discussion - Function Overloading

#include <iostream> //used to display to the user
using namespace std; //using std namespace

long int mathRun (long int longOne, long int longTwo) //creates mathRun function with long int parameters
{
	return (longOne + longTwo); //returns the variables added
}
unsigned int mathRun (unsigned int unOne, unsigned int unTwo) //creates mathRun function with unsigned int parameters
{
	return (unOne * unTwo); //returns the variables multiplies
}
int main () //begins the program
{
	long int x = 102, y = 516; //assigning long integer numbers to the variables x and y
	unsigned int a = 156, b = 39; //assigning unsigned integers to the variables a and b
	cout << mathRun (x, y); //since x and y are 'long int' values it will run through the long int section on line 4
	cout << "\n"; //new line so numbers don't run together
	cout << mathRun (a, b); // since a and b are 'unsigned int' values it will run through the unsigned int section on line 8
	cout << "\n"; //new line so the pause text doesn't run together with the numbers
system("pause"); //put to leave the window up
}

//OUTPUT
//long int x = 102, y = 516
//102 + 516 = 618
//
//unsigned int a = 156, b = 39
//156 * 39 = 6084