/*
Student: Clinton Booze
Professor: Jill Coddington
Week 4 - Assignment - 15.25
#################################################################################################
(Sieve of Eratosthenes)
Modify Exercise 15.24, the Sieve of Eratosthenes, so that, if the number the user inputs into the
program is not prime, the program displays the prime factors of the number. Remember that a prime
number�s factors are only 1 and the prime number itself. Every nonprime number has a unique prime
factorization. For example, the factors of 54 are 2, 3, 3 and 3. When these values are multiplied
together, the result is 54. For the number 54, the prime factors output should be 2 and 3.
#################################################################################################*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <bitset>
using namespace std;

//declaring functions
void factor (int input);
void vectorDisplay(const vector<int> &myVector);

int main()
{
	//declaring
	const int bitSize = 1024;
	int input;
	bitset< bitSize > sieve; //default all set to false(0)
	sieve.flip(); //flips them all to be true(1)
	sieve.reset(0); //resets element 0 to false
	sieve.reset(1); //resets element 1 to false

	//gets 33 so it will run through by multiples of 2-33
	//to ensure all numbers are removed that are not primes
	int finalBit = sqrt(sieve.size()) + 1;

	//itterates from 2-33
	for (int i = 2; i < finalBit; ++i)
	{
		if(sieve.test(i))//if bit is true
		{
			for (int j = 2 * i; j < bitSize; j += i)
				sieve.reset(j); //set element to false
		}
	}

	cout << "Primes between 2 and 1023 are:" << endl;
	// display prime numbers from 2-1023
	for (int k = 2, counter = 1; k < bitSize; ++k)
	{
		if(sieve.test(k))//if k is true
		{
			cout << setw(5) << k; //for orginization

			//lists 10 numbers then goes to the next line
			if (counter++ % 10 == 0)
				cout << '\n';
		}
	}
	cout << endl;

	// get value from user to determine whether value is prime
	cout << "\nEnter a number between 2 and 1023 (0 to exit): ";
	cin >> input;

	// determine whether user input is prime
	while (input != 0)
	{
		if(sieve[input])//if location is true
		{
			cout << input << " is a prime number\n";
		}
		else // not a prime number
		{
			cout << input << " is not a prime number\n";
			factor(input);
		}
		cout << "\nEnter a number between 2 and 1023 (0 to exit): ";
		cin >> input;
	}
}

//function to get the factors of a non prime number
void factor (int input)
{
	//declaring
	int it = 2;
	vector<int> myVector;

	//if itterator is more than half the number it exits loop
	while ((it * it) <= input)
	{
		//if the itterator is a multiple of the value
		if (input % it == 0)
		{
			//modify the value to remove the factor
			input /= it;
			//==0 to catch out of range || to avoid duplications
			if (myVector.size() == 0 || myVector[myVector.size()-1] != it)
			{
				//add to back of vector
				myVector.push_back(it);
			}
		}else
		{
			it++;
		}
	}
	//add the remainder if greater than 1 || to avoid duplication
	if (input > 1 && myVector[myVector.size()-1] != input)
	{
		//add remainder to back
		myVector.push_back(input);
	}
	//display vector
	vectorDisplay(myVector);
}

//function to display vector elements
void vectorDisplay(const vector<int> &myVector)
{
	cout << "Factors = ";
    copy(myVector.begin(), myVector.end(), ostream_iterator<int>(cout, " "));
}