#ifndef LINKLIST_H
#define LINKLIST_H

#include <iostream>
#include <stdlib.h>
using namespace std;

template <class T> class LinkList
{

//structure containing node info
public:
struct LinkListItem {
	LinkListItem *next;
	LinkListItem *previous;
	T element;
};

//private variables
private:
	LinkListItem *first;
	LinkListItem *last;
	int linklistSize;
	unsigned long flips;
	T minElement;
	T maxElement;

//public methods
public:
	LinkList(void);
	virtual ~LinkList(void);

	LinkListItem *nodeFind(T element);
	void nodeInsert(T object);
	T nodeDelete(LinkListItem *object);
	T nodeMin();
	T nodeMax();
	int nodeCount();
	void listClear();
	void print_list();

	unsigned long get_flips();

	void bubble_sort();
	void selection_sort();
	void insert_sort();
};

//header
//#####################################################
//cpp

//constructor
template <class T> LinkList<T>::LinkList(void){
	this->linklistSize = 0;
	this->first = 0;
	this->last = 0;
}

//insert a new node into the linked list
template <class T> void LinkList<T>::nodeInsert(T object){
	LinkListItem *newElement = new LinkListItem;
	newElement->element = object;
	newElement->next = 0;
	newElement->previous = this->last;

	//checking for max element
	if (object > this->maxElement){
		this->maxElement = object;
	} //end if

	//checking for min element
	if (object < this->minElement){
		this->minElement = object;
	} //end if

	//checking if list is empty
	if (this->linklistSize == 0){
		this->first = newElement;
		this->minElement = object;
		this->maxElement = object;
		newElement->previous = NULL;
	}else{
		this->last->next = newElement;
	}//end if

	//updating variables
	this->last = newElement;
	this->linklistSize++;
} //end nodeInsert

//deleting a node from the list
template <class T> T LinkList<T>::nodeDelete(LinkListItem *object){
	//saving information from soon to be deleted node
	T element = object->element;
	LinkListItem *before = object->previous;
	LinkListItem *after = object->next;

	//modifying adjacent nodes
	before->next = after;
	after->previous = before;

	//deleting node
	delete object;

	//updating list count and returning deleted node info
	this->linklistSize--;
	return element;
} //end nodeDelete

//search the list to find a specific node based off of element
template <class T> typename LinkList<T>::LinkListItem * LinkList<T>::nodeFind(T element){
	//creating temp variables
	int listSize = this->linklistSize;
	LinkListItem *temp = new LinkListItem;
	temp = this->first;

	//searching the list
	while (listSize > 0){
		if (temp->element == element){
			return temp;
		} //end if
		temp = temp->next;
		listSize--;
	} //end while
	return 0;
} //end nodeFind

//returns the minimum value element
template <class T> T LinkList<T>::nodeMin(){
	return this->minElement;
} //end nodeMin

//returns the max value element
template <class T> T LinkList<T>::nodeMax(){
	return this->maxElement;
} //end nodeMax

//clearing the list freeing up the memory
template <class T> void LinkList<T>::listClear(){
	while(this->linklistSize > 0){
		LinkListItem *temp = this->first->next;
		delete this->first;
		this->first = temp;
		this->linklistSize--;
	} //end while
} //end listClear

//returning the size of the list
template <class T> int LinkList<T>::nodeCount(){
	return this->linklistSize;
} //end nodeCount

//itterates through the list and prints it
template <class T> void LinkList<T>::print_list(){
	LinkListItem *temp = first;
	bool should_exit = false;
	while (!should_exit){
		cout << temp->element << " ";
		if (temp->next == NULL){
			should_exit = true;
		}else{
			temp = temp->next;
		} //end if
	} //end while
} //end print_list

//returns the number of flips made durring the sorting process
template <class T> unsigned long LinkList<T>::get_flips(){
	return this->flips;
} //end get_insert_flips

//destructor
template <class T> LinkList<T>::~LinkList(void){
	this->listClear();
}

//cpp
//#####################################################
//bubble sort

//sorts the list using bubble sort
template <class T> void LinkList<T>::bubble_sort(){
	LinkListItem *current_node = new LinkListItem;
	LinkListItem *sorted_node = new LinkListItem;
	sorted_node->previous = last;
	this->flips = 0;
	
	bool should_exit = false;
	T temp_element;

	while (!should_exit){
		should_exit = true;
		current_node = first;
		sorted_node = sorted_node->previous;
		while (current_node != sorted_node){
			if (current_node->element > (current_node->next)->element){
				temp_element = current_node->element;
				current_node->element = (current_node->next)->element;
				(current_node->next)->element = temp_element;
				should_exit = false;
				this->flips++;
			}
			current_node = current_node->next;
		}
	}
} //end bubble_sort

//bubble sort
//#####################################################
//selection sort

//sorts the list using selection sort
template <class T> void LinkList<T>::selection_sort(){
	this->flips = 0;
	LinkListItem *i, *j, *lowest;

	for(i = first; i != NULL && i->next != NULL; i = i->next){
		lowest = i;
		for(j = i->next; j != NULL ; j = j->next){
			if(j->element < lowest->element)
				lowest = j;
		}
		if(lowest != i){
			int temp;
			temp = lowest->element;
			lowest->element = i->element;
			i->element = temp;
			this->flips++;
		}
	}
} //end selection_sort

//selection sort
//#####################################################
//insert sort

//sorts the list using insertion sort
template <class T> void LinkList<T>::insert_sort(){
	LinkListItem *it = new LinkListItem;
	LinkListItem *head = first;
	T temp;
	this->flips = 0;

	for (head; head != NULL; head = head->next){
		it = head;
		while (it->previous != NULL && (it->previous)->element > it->element){
			temp = it->element;
			it->element = (it->previous)->element;
			(it->previous)->element = temp;
			it = it->previous;
			this->flips++;
		}
	}
} //end insert_sort

//insert sort
//#####################################################

#endif