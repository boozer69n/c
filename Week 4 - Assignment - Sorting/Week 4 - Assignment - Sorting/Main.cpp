/*
Arthur: Clinton Booze
Professor: Jason Turner
Week 4 - Assignment - Sorting
-----------------------------------------------
Assignment
Sorting
Implementation
Implement at least 3 sorting algorithms of your choice (for example, insertion sort, merge sort, quick sort) that operates on your linked list data structure from the first code assignment.
Your program should take two arguments, the filename of a data file, and an integer that defines the algorithm that will be used to sort.

The data file contains one integer per line, for an unknown number of lines.  Your program should parse this data file into your linked list structure, and then use the specified algorithm to sort the array.  Your program should time how long it takes to sort the data, and report that information to the console in seconds.  Take care to only report the time it takes to sort, not perform file i/o.  For small dataset sizes, you may have to average several consecutive sorts in order to arrive at a valid timing measurement.
HINT: Use the “clock()” - system call:
http://www.cplusplus.com/reference/clibrary/ctime/clock/

If the size of the dataset is less than 100, your program should print the sorted list to the console screen.

On the course shell in Doc Sharing, there is a file called “SortDatasets.zip” containing several datasets.  For each dataset in the file, run each sorting algorithm, and record the times in an Excel table.  For example:


Once all of your data is collected, graph the performance of each algorithm by data set size, and use a trend-line to determine the growth-rate of each algorithm.
Lab Report
In Word or a similar word processing program, write a Lab Report including the following sections:
   •    Introduction – In your own words, describe the operation of each of your 3 sorting algorithms.
   •    Programmer’s Guide  – Provide detailed documentation describing your implementation of sorts.  For each function, give a usage example.  Also give documentation on the general usage of your program.
   •    Analysis – Determine the worst-case time complexity of the 3 algorithms you chose, and document your calculations in this section.  Additionally, include your data table and graphs.  In this section, answer the following questions:
   ◦    Does your calculated run-time complexity match the measured run-time complexity?  Why or why not?
   ◦    Under which dataset sizes does each algorithm perform best?  Why or why not?  Does any algorithm perform best under all cases?
Submission
Submit a *.zip file containing your lab report and code project folder to the drop-box associated with this assignment.  Name the *.zip file with your Last Name, and the week.  For example:
DoeWeek4.zip
*/




#include <iostream>
#include <fstream>
#include <ctime>
#include <time.h>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include "LinkList.h"
#include <string>

using namespace std;

LinkList<int> my_list;
clock_t start_time;
clock_t end_time; //yes I said end time!

ofstream results("results.txt",ios::out | ios::app);
ifstream file_10("num10.dat",ios::in);
ifstream file_100("num100.dat",ios::in);
ifstream file_1000("num1000.dat",ios::in);
ifstream file_10000("num10000.dat",ios::in);
ifstream file_100000("num100000.dat",ios::in);
ifstream file_1000000("num1000000.dat",ios::in);

void bubble_test();
void selection_test();
void insert_test();
void save_stats(string list_name);

void fill_list_10(LinkList<int> &list);
void fill_list_100(LinkList<int> &list);
void fill_list_1000(LinkList<int> &list);
void fill_list_10000(LinkList<int> &list);
void fill_list_100000(LinkList<int> &list);
void fill_list_1000000(LinkList<int> &list);

int main(){
	int choice;
	cout << "Select Sort Method" << endl;
	cout << "1 - Bubble Sort" << endl;
	cout << "2 - Selection Sort" << endl;
	cout << "3 - Insertion Sort" << endl;
	cout << "0 - Exit" << endl;
	cout << "Choice: ";
	cin >> choice;

	switch (choice){
	case 1:
		bubble_test();
		break;
	case 2:
		selection_test();
		break;
	case 3:
		insert_test();
		break;
	default:
		break;
	}
	return 0;
}

void bubble_test(){
	int stop;
	//10
	cout << "\nRunning Size 10" << endl;
	my_list.listClear();
	fill_list_10(my_list);
	
	cout << "\nBefore" << endl;
	my_list.print_list();

	start_time = clock();
	my_list.bubble_sort();
	end_time = clock();

	cout << "\nAfter" << endl;
	my_list.print_list();
	save_stats("Bubble");
	cout << "\nStop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//100
	cout << "\nRunning Size 100" << endl;
	my_list.listClear();
	fill_list_100(my_list);
	start_time = clock();
	my_list.bubble_sort();
	end_time = clock();
	save_stats("Bubble");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//1,000
	cout << "\nRunning Size 1,000" << endl;
	my_list.listClear();
	fill_list_1000(my_list);
	start_time = clock();
	my_list.bubble_sort();
	end_time = clock();
	save_stats("Bubble");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//10,000
	cout << "\nRunning Size 10,000" << endl;
	my_list.listClear();
	fill_list_10000(my_list);
	start_time = clock();
	my_list.bubble_sort();
	end_time = clock();
	save_stats("Bubble");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//100,000
	cout << "\nRunning Size 100,000" << endl;
	my_list.listClear();
	fill_list_100000(my_list);
	start_time = clock();
	my_list.bubble_sort();
	end_time = clock();
	save_stats("Bubble");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//1,000,000
	cout << "\nRunning Size 1,000,000" << endl;
	my_list.listClear();
	fill_list_1000000(my_list);
	start_time = clock();
	my_list.bubble_sort();
	end_time = clock();
	save_stats("Bubble");
	cout << "Finished and Stored" << endl;
	system("pause");
}

void selection_test(){
	int stop;
	//10
	cout << "\nRunning Size 10" << endl;
	my_list.listClear();
	fill_list_10(my_list);
	
	cout << "\nBefore" << endl;
	my_list.print_list();

	start_time = clock();
	my_list.selection_sort();
	end_time = clock();

	cout << "\nAfter" << endl;
	my_list.print_list();
	save_stats("Selection");
	cout << "\nStop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//100
	cout << "\nRunning Size 100" << endl;
	my_list.listClear();
	fill_list_100(my_list);
	start_time = clock();
	my_list.selection_sort();
	end_time = clock();
	save_stats("Selection");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//1,000
	cout << "\nRunning Size 1,000" << endl;
	my_list.listClear();
	fill_list_1000(my_list);
	start_time = clock();
	my_list.selection_sort();
	end_time = clock();
	save_stats("Selection");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//10,000
	cout << "\nRunning Size 10,000" << endl;
	my_list.listClear();
	fill_list_10000(my_list);
	start_time = clock();
	my_list.selection_sort();
	end_time = clock();
	save_stats("Selection");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//100,000
	cout << "\nRunning Size 100,000" << endl;
	my_list.listClear();
	fill_list_100000(my_list);
	start_time = clock();
	my_list.selection_sort();
	end_time = clock();
	save_stats("Selection");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//1,000,000
	cout << "\nRunning Size 1,000,000" << endl;
	my_list.listClear();
	fill_list_1000000(my_list);
	start_time = clock();
	my_list.selection_sort();
	end_time = clock();
	save_stats("Selection");
	cout << "Finished and Stored" << endl;
	system("pause");
}

void insert_test(){
	int stop;
	//10
	cout << "\nRunning Size 10" << endl;
	my_list.listClear();
	fill_list_10(my_list);
	
	cout << "\nBefore" << endl;
	my_list.print_list();

	start_time = clock();
	my_list.insert_sort();
	end_time = clock();

	cout << "\nAfter" << endl;
	my_list.print_list();
	save_stats("Insertion");
	cout << "\nStop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//100
	cout << "\nRunning Size 100" << endl;
	my_list.listClear();
	fill_list_100(my_list);
	start_time = clock();
	my_list.insert_sort();
	end_time = clock();
	save_stats("Insertion");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//1,000
	cout << "\nRunning Size 1,000" << endl;
	my_list.listClear();
	fill_list_1000(my_list);
	start_time = clock();
	my_list.insert_sort();
	end_time = clock();
	save_stats("Insertion");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//10,000
	cout << "\nRunning Size 10,000" << endl;
	my_list.listClear();
	fill_list_10000(my_list);
	start_time = clock();
	my_list.insert_sort();
	end_time = clock();
	save_stats("Insertion");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//100,000
	cout << "\nRunning Size 100,000" << endl;
	my_list.listClear();
	fill_list_100000(my_list);
	start_time = clock();
	my_list.insert_sort();
	end_time = clock();
	save_stats("Insertion");
	cout << "Finished and Stored" << endl;
	cout << "Stop? (0-Yes, 1-No): ";
	cin >> stop;
	if(stop == 0){return;}

	//1,000,000
	cout << "\nRunning Size 1,000,000" << endl;
	my_list.listClear();
	fill_list_1000000(my_list);
	start_time = clock();
	my_list.insert_sort();
	end_time = clock();
	save_stats("Insertion");
	cout << "Finished and Stored" << endl;
	system("pause");
}

void fill_list_10(LinkList<int> &list){
	if (file_10.good()){
		string str;
		int num;
		while(getline(file_10, str)){
			istringstream ss(str);
			while(ss >> num){
				list.nodeInsert(num);
			}
		}
	}
	file_10.close();
} //end fill_list_10

void fill_list_100(LinkList<int> &list){
	if (file_100.good()){
		string str;
		int num;
		while(getline(file_100, str)){
			istringstream ss(str);
			while(ss >> num){
				list.nodeInsert(num);
			}
		}
	}
	file_100.close();
} //end fill_list_100

void fill_list_1000(LinkList<int> &list){
	if (file_1000.good()){
		string str;
		int num;
		while(getline(file_1000, str)){
			istringstream ss(str);
			while(ss >> num){
				list.nodeInsert(num);
			}
		}
	}
	file_1000.close();
} //end fill_list_1000

void fill_list_10000(LinkList<int> &list){
	if (file_10000.good()){
		string str;
		int num;
		while(getline(file_10000, str)){
			istringstream ss(str);
			while(ss >> num){
				list.nodeInsert(num);
			}
		}
	}
	file_10000.close();
} //end fill_list_10000

void fill_list_100000(LinkList<int> &list){
	if (file_100000.good()){
		string str;
		int num;
		while(getline(file_100000, str)){
			istringstream ss(str);
			while(ss >> num){
				list.nodeInsert(num);
			}
		}
	}
	file_100000.close();
} //end fill_list_100000

void fill_list_1000000(LinkList<int> &list){
	if (file_1000000.good()){
		string str;
		int num;
		while(getline(file_1000000, str)){
			istringstream ss(str);
			while(ss >> num){
				list.nodeInsert(num);
			}
		}
	}
	file_1000000.close();
} //end fill_list_1000000

void save_stats(string list_name){
	results << list_name << " Sort - Size: " << my_list.nodeCount() << " - Time: " << (end_time - start_time) * .001 << " - Flips: " << my_list.get_flips() << endl;
}