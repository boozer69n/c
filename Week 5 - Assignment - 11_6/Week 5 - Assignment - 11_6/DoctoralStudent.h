//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef DOCTORALSTUDENT_H //for compatibility
#define DOCTORALSTUDENT_H //for compatibility

#include "graduatestudent.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class DoctoralStudent : public GraduateStudent //class and sub-classed too
{
public:
	DoctoralStudent(bool cramFinal = 0, //cram for finals
					bool buyBook = 0); //buy book for upcoming class
	virtual ~DoctoralStudent(void); //destructor

	void cramFinal (bool cramFinal); //cram for final
	void buyBook (bool buyBook); //cram for final

	virtual void print() const; //display

private:
	bool cramFinal; //cram for final
	bool buyBook; //buy book

};

#endif //DOCTORALSTUDENT_H