//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef UNDERGRADUATESTUDENT_H //for compatibility
#define UNDERGRADUATESTUDENT_H //for compatibility

#include "student.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class UndergraduateStudent : public Student //class and sub-classed too
{
public:
	UndergraduateStudent(bool tA = 0);
	virtual ~UndergraduateStudent(void); //destructor

	void setTa (const string &tA); //setting teachers assistant
	string getTa () const; //getting teachers assistant

	virtual void print() const; //print

private:
	bool tA; //teachers assistant

};

#endif //UNDERGRADUATESTUDENT_H