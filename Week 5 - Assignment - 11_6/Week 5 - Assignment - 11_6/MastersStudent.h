//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef MASTERSSTUDENT_H //for compatibility
#define MASTERSSTUDENT_H //for compatibility

#include "graduatestudent.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class MastersStudent : public GraduateStudent //class and sub-classed too
{
public:
	MastersStudent();
	virtual ~MastersStudent(); //destructor

	//overloaded for possible inputs
	void setNickname (const string &nickname); //nickname as string
	void setNickname (int nickname); //nickname as integer
	void setNickname (double nickname); //nickname as double
	string getNickname () const; //getting nickname

	virtual void print() const; //display

private:
	string nickname; //nickname variable

};

#endif //MASTERSSTUDENT_H