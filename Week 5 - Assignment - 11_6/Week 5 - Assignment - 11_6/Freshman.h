//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef FRESHMAN_H //for compatibility
#define FRESHMAN_H //for compatibility

#include "undergraduatestudent.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class Freshman : public UndergraduateStudent //class and sub-classed too
{
public:
	Freshman(double hsGpa = 0.0, //high school gpa
			 int suspensions = 0); //number of suspensions
	virtual ~Freshman(void); //destructor

	void sethsGpa (double hsGpa); //sets high school gpa
	double gethsGpa () const; //gets high school gpa

	void setSuspensions (int suspensions); //sets suspensions
	int getSuspensions () const; //gets suspensions

	void goParty () const; //going to party

	virtual void print() const; //display

private:
	double hsGpa; //high school gpa
	int suspensions; //number of suspensions

};

#endif //FRESHMAN_H