//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef GRADUATESTUDENT_H //for compatibility
#define GRADUATESTUDENT_H //for compatibility

#include "student.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class GraduateStudent : public Student //class and sub-classed too
{
public:
	GraduateStudent(const string &dissertation, //dissertation
					const string &intern, //interned where
					const string &interviewDate, //interview date
					const string &committee, //committee
					bool deansList = 0);
	virtual ~GraduateStudent(void); //destructor

	void setDissertation (const string &dissertation); //setting dissertation
	string getDissertation () const; //getting dissertation

	void setIntern (const string &intern); //setting where interned
	string getIntern () const; //getting where interned

	void setInterview (const string &interview); //setting interview date
	string getInterview () const; //getting interview date

	void setCommittee (const string &committee); //setting committee
	string getCommittee () const; //getting committee

	void setDeansList (bool deansList); //setting deans list
	bool deansList () const; //getting deans list

	virtual void print() const; //display

private:
	string dissertation; //dissertation
	string intern; //intern where
	string interviewDate; //interview date
	string committee; //committee name
	bool deansList; //deans list t/f

};

#endif //GRADUATESTUDENT_H