//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef SOPHOMORE_H //for compatibility
#define SOPHOMORE_H //for compatibility

#include "undergraduatestudent.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class Sophomore : public UndergraduateStudent //class and sub-classed too
{
public:
	Sophomore(const string &homework);
	~Sophomore(void); //destructor

	void setHomework (const string &homework); //setting homework assignment
	string getHomework () const; //getting homework assignment

	virtual void print() const; //display

private:
	string homework; //homework assignment

};

#endif //SOPHOMORE_H