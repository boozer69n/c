//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef STUDENT_H //for compatibility
#define STUDENT_H //for compatibility

#include <string> //adding strings
using std::string; //adding strings

class Student //base class
{
public: //available everywhere
	Student(const string &first, //first name
			const string &last, //last name
			const string &ssn, //social security number
			const string &idNum, //school id number
			const string &major, //current major if any
			const string &minor, //current minor if any
			const string &payMethod, //current payment method ex. scholarship/loans
			double tCredits = 0.0, //total credits
			double rCredits = 0.0, //remaining credits
			double gpa = 0.0, //current gpa
			double attendance = 0.0); //attendance record
	virtual ~Student(); //destructor

	void setFirst (const string &first); //setting first name
	string getFirst () const; //getting/returning first name

	void setLast (const string &last); //setting last name
	string getLast () const; //getting/returning last name

	void setSsn (const string &ssn); //setting social
	string getSsn () const; //getting/returning social

	void setIdnum (const string &idNum); //setting id number
	string getIdnum () const; //getting/returning id number

	void setMajor (const string &major); //setting major
	string getMajor () const; //getting/returning major

	void setMinor (const string &minor); //setting minor
	string getMinor () const; //getting/returning minor

	void setPayMethod (const string &payMethod); //setting payment method
	string getPayMethod () const; //getting/returning payment method

	void settCredits (double tCredits); //setting total credits
	double gettCredits () const; //getting/returning total credits

	void setrCredits (double rCredits); //setting remaining credits
	double getrCredits () const; //getting/returning remaining credits

	void setGpa (double gpa); //setting gpa
	double getGpa () const; //getting/returning gpa

	void setAttendance (double &attendance); //setting attendance
	double getAttendance () const; //getting/returning attendance

	virtual void print () const; //print

protected:
	//overloaded due to multiple types of input
	void check (double input); //to run checks on input
	void check (int input); //to run checks on input
	void check (string input); //to run checks on input

private:
	string firstName; //first name
	string lastName; //last name
	string ssn; //social number
	string idNum; //school id number
	string major; //major
	string minor; //minor
	string payMethod; //payment method
	double tCredits; //total credits
	double rCredits; //remaining credits
	double gpa; //gpa
	double attendance; //attendance
};

#endif //STUDENT_H