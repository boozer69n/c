//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef CHECK_H //for compatibility
#define CHECK_H //for compatibility

#include <stdexcept> //for errors
#include <string> //adding strings

using std::string; //to reduce memory
using std::runtime_error; //to reduce memory

class Check : public runtime_error //class and sub-classed too
{
public:
	//receive error send up to runtime_error, then exception
	explicit Check(const string& what) : runtime_error(what) {}
	//receive error send up to runtime_error, then exception
	explicit Check(const char* what) : runtime_error(what) {}

};

#endif //CHECK_H