//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef SENIOR_H //for compatibility
#define SENIOR_H //for compatibility

#include "undergraduatestudent.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class Senior : public UndergraduateStudent //class and sub-classed too
{
public:
	Senior(bool dissectFrog = 0, bool mixChemicals = 0);
	virtual ~Senior(void); //destructor

	void setDissectFrog (bool dissectFrog); //setting frog dissection
	bool dissectFrog () const; //getting frog dissection

	void setMixChemicals (bool mixChemicals); //setting chemical mixing
	bool getMixChemicals () const; //getting chemical mixing

	virtual void print() const; //displaying

private:
	bool dissectFrog; //dissecting frog
	bool mixChemicals; //mixing chemicals

};

#endif //SENIOR_H