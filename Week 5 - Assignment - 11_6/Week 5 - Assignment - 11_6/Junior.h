//Student: Clinton Booze
//Professor: Victoria Heil
//Week 5 - Assignment - 11.6
//#################################################################################################

#ifndef JUNIOR_H //for compatibility
#define JUNIOR_H //for compatibility

#include "undergraduatestudent.h" //loading header
#include <string> //adding strings
using std::string; //adding strings

class Junior : public UndergraduateStudent //class and sub-classed too
{
public:
	Junior(const string &physicalEd);
	~Junior(void); //destructor

	void setPhysicalEd (const string &physicalEd); //type of physical ed
	string getPhysicalEd () const; //getting physical ed name

	virtual void print() const; //display

private:
	string physicalEd; //physical education

};

#endif //JUNIOR_H