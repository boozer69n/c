/*
Student: Clinton Booze
Professor: Jill Coddington
Week 1 - Assignment - 17.26
//#################################################################################################
(Catching All Exception)
Write a program that demonstrates several exception types being caught with the
catch(...) exception handler
//#################################################################################################
*/
#include <stdexcept> //for errors
#include <iostream> //for input/output
#include <array> //to use arrays
using namespace std; //std namespace

//declaring
void outOfRange ();
int invalidArgument ();
int divideByZeroError(int x, int y);

int main() //main
{ //begin main
	try
	{ //begin try
		outOfRange(); //out of Range function
	} //end try
	catch(...) //ellipsis catches all
	{ //start catch
		cout << "Caught\n"; //displaying error was caught
	} //end catch

	try
	{ //begin try
		invalidArgument(); //invalid argument function
	} //end try
	catch(...) //ellipsis catches all
	{ //start catch
		cout << "Caught\n"; //displaying error was caught
	} //end catch

	try
	{ //begin try
		int zeroIntOne; //creating int variable
		int zeroIntTwo; //creating int variable
		int divisionAnswer; //creating int variable
		zeroIntOne = 5; //defining variable
		zeroIntTwo = 0; //defining variable
		divisionAnswer = divideByZeroError(zeroIntOne, zeroIntTwo); //attempting to divide
	} //end try
	catch(...) //ellipsis catches all
	{ //start catch
		cout << "Caught\n"; //displaying error was caught
	} //end catch
	system("pause"); //to keep window up
}
//#################################################################################################
void outOfRange () //creating a function to throw an out of range error
{
	cout << "\nOut Of Range Error" << endl; //displaying the type of error
	array<int,6> myArray; //creating an int array with 0-5 slots
	myArray.fill(5); //filling the array
	cout << myArray.at(6); //attempting to display the 6th slot of the array
						   //have to use the .at to throw an exception with
						   //this type of an array
}
//#################################################################################################
int invalidArgument () //invalid argument function
{ //begin function
	cout << "\nInvalid Argument Error" << endl; //displaying the type of error
	int intVarOne; //creating int variable
	int intVarTwo; //creating int variable
	intVarOne = 5; //defining variable
	intVarTwo = -8; //defining variable
	if(intVarOne < 0 || intVarTwo < 0) //checking to ensure both are positive
	{ //begin if
		throw invalid_argument("All numbers must be positive"); //throws if a number is negative
	} //end if
	else
	{ //begin else
		return intVarOne + intVarTwo; //return the numbers added
	} //end else
}
//#################################################################################################
int divideByZeroError(int x, int y) //divide by zero function
{ //begin function
	cout << "\nDivide By Zero Error" << endl; //displaying error type
	if (y == 0) //checking denominator
	{ //begin if
		throw overflow_error("Cannot Divide By Zero"); //thrown if denominator is 0
	} //end if
	else
	{ //begin else
		return x / y; //return answer
	} //end else
}
//#################################################################################################