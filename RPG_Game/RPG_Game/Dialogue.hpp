#ifndef DIALOGUE_HPP
#define DIALOGUE_HPP
 
#include <string>
#include <vector>
#include <iostream>

using std::string;
using std::vector;
using std::cout;
using std::cin;
using std::endl;
 
class Dialogue
{
private:
	string description;
	vector<string> choices;

public: 
	Dialogue(){}
	
	//sets the interaction and the possible choices
	Dialogue(string description, vector<string> choices){
		this->description = description;
		this->choices = choices;
	}
	
	//display choices and return choice
	int activate(){
		cout << description << endl;
 
		// Output and number the choices
		for(int i = 0; i < this->choices.size(); ++i){
			cout << "[" << i + 1 << "] " << this->choices[i] << endl;
		}
 
		int userInput = -1;
		// loop until valid input
		while(true){
			cin >> userInput;
			//within the range of numbers outputted
			if(userInput >= 0 && userInput <= choices.size()){
				return userInput;
			}
		} 
		return 0;
	}
}; 
#endif