#ifndef ARMOR_HPP
#define ARMOR_HPP
 
#include "item.hpp"
#include <string>

class Armor : public Item
{
private:
	int defense; //1-50

public:
    //slots
    enum Slot {TORSO, HEAD, LEGS, N};
    Slot slot;
 
    Armor(string name, string description, int defense, Armor::Slot slot):
	  Item(name, description){
        this->defense = defense;
        this->slot = slot;
    }
 
	Armor(){}


	void setDefense(int defense){
		this->defense = defense;
	}
	int getDefense(){
		return this->defense;
	}
}; 
#endif