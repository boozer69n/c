#include "atlas.hpp"
 
void buildatlas_creature(vector<Creature>& atlas){
    //fill atlas with creatures
    atlas.push_back(Creature("Rat", 8, 8, 8, 12, 2.0, 1));
     return;
}
 
void buildatlas_item(vector<Item>& atlas){
    //item(Name, Description)
    atlas.push_back(Item("Gold Coin", "A small disc made of lustrous metal"));
    atlas.push_back(Item("Iron Key",  "A heavy iron key with a simple cut"));
    return;
}
 
void buildatlas_weapon(vector<Weapon>& atlas){
    //weapon(name, description, dmg, hit rate)
    atlas.push_back(Weapon("Iron Dagger",
						   "A short blade made of iron with a leather-bound hilt",
						   5, 10.0));
    atlas.push_back(Weapon("Excalibur",
						   "The legendary blade, bestowed upon you by the Lady of the Lake",
						   35, 35.0));
    return;
}
 
void buildatlas_armor(vector<Armor>& atlas){
    //armor(name, description, def, slot)
    atlas.push_back(Armor("Leather Cuirass", "Torso armor made of tanned hide",
						  4, Armor::Slot::TORSO));
    return;
}
 
void buildatlas_area(vector<Area>& atlas, vector<Item>& items,
					 vector<Weapon>& weapons, vector<Armor>& armor,
					 vector<Creature>& creatures){
    //area definitions
    atlas.push_back(Area(Dialogue(        // Standard dialogue definition
        "You are in room 1",              // Description
        {"Go to room 2", "Search"}),      // Choices
        Inventory({                       // Area inventory
            make_pair(&items[0], 5)  // Pair of item and quantity
        },
        {
            make_pair(&weapons[0], 1)// Pair of weapon and quantity
        },
        {
            make_pair(&armor[0], 1) // Pair of armor and quantity
        }),
        {                                 // Creatures
        }));
 
    atlas.push_back(Area(Dialogue(
        "You are in room 2",
        {"Go to room 1", "Search"}),
        Inventory({
            make_pair(&items[0], 10),
            make_pair(&items[1], 1)
        },
        {
        },
        {
        }),
        {
            &creatures[0]
        })); 
    return;
}