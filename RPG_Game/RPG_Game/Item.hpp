#ifndef ITEM_HPP
#define ITEM_HPP
 
#include <string>
using namespace std;
 
class Item
{
private:
	string name;
	string description;
public:
	Item(string name, string description){
		this->name = name;
		this->description = description;
	}

	void setName(string name){
		this->name = name;
	}
	string getName(){
		return this->name;
	}

	void setDescription(string description){
		this->description = description;
	}
	string getDescription(){
		return this->description;
	}
}; 
#endif