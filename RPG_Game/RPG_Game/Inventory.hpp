#ifndef INVENTORY_HPP
#define INVENTORY_HPP
 
#include "item.hpp"
#include "weapon.hpp"
#include "armor.hpp"
 
#include <list>
#include <utility>
#include <iostream>
 
class Inventory
{
private:
	list<pair<Item*, int>> items;
    list<pair<Weapon*, int>> weapons;
    list<pair<Armor*, int>> armor;
public:
    Inventory(){}
 
    Inventory(list<pair<Item*, int>> items,
			  list<pair<Weapon*, int>> weapons,
			  list<pair<Armor*, int>> armor){
        this->items = items;
        this->weapons = weapons;
        this->armor = armor;
    }

	//adding item to the inventory
	void add_item(Item* item, int count){
		//iterate through list
		for(auto &it : this->items){
			//if item is found it adds the number gained (count)
			if(it.first == item){
				it.second += count;
			}
			return;
		}
		//if item doesnt exist then add it
		this->items.push_back(make_pair(item, count));
	}
	
	//adding a weapon to the inventory
	void add_weapon(Weapon* weapon, int count){
		//iterating through list
		for(auto& it : this->weapons){
			if(it.first == weapon){
				it.second += count;
				return;
			}
		}
		this->weapons.push_back(make_pair(weapon, count));
	}
	
	//adding armor to the inventory
	void add_armour(Armor* armor, int count){
		//iterating through list
		for(auto &it : this->armor){
			if(it.first == armor){
				it.second += count;
				return;
			}
		}
		this->armor.push_back(make_pair(armor, count));
	}

	//remove an item from the inventory
	void remove_item(Item* item, int count){
		//iterate through list and reduce amount by 1 if match is found
		for(auto &it : this->items){
			if(it.first == item){
				it.second -= count;
			}
		}
		//lambda through inventory and remove any that have 0 count
		this->items.remove_if([](pair<Item*, int> &element){
			return element.second < 1;
		});
	}
	
	//remove weapon
	void remove_weapon(Weapon* weapon, int count){
		//iterate through list
		for(auto& it : this->weapons){
			if(it.first == weapon){
				it.second -= count;
			}
		}
		//lambda through inventory and remove any that have 0 count
		this->weapons.remove_if([](pair<Weapon*, int> &element){
			return element.second < 1;
		});
	}
	 
	//remove armor
	void remove_armor(Armor* armor, int count){
		//iterate through list
		for(auto& it : this->armor){
			if(it.first == armor){
				it.second -= count;
			}
		}
		//lambda through inventory and remove any that have 0 count
		this->armor.remove_if([](pair<Armor*, int> &element){
			return element.second < 1;
		});
	}

	//merging inventories
	void merge(Inventory* inventory){
		//cant merge an inventory with itself
		if(inventory == this){
			return;
		}	 
		//loop through and add
		for(auto it : inventory->items){
			this->add_item(it.first, it.second);
		}
		//loop through and add
		for(auto it : inventory->weapons){
			this->add_weapon(it.first, it.second);
		}
		//loop through and add
		for(auto it : inventory->armor){
			this->add_armor(it.first, it.second);
		}	 
		return;
	}

	//clear inventory
	void clear(){
		this->items.clear();
		this->weapons.clear();
		this->armor.clear();
	}
	
	//display item
	int print_items(bool label = false){
		unsigned int i = 1;
	 
		for(auto it : this->items){
			//number the items if asked
			if(label){
				//display name, qty, and description
				cout << "[" << i++ << "] "
					 << it.first->name << " (" << it.second << ") - "
					 << it.first->description << endl;
			 }
		}	 
		//return the number of items outputted, for convenience
		return this->items.size();
	}
	 
	//display weapon
	int print_weapons(bool label = false){
		unsigned int i = 1;
	 
		for(auto it : this->weapons){
			if(label){
				cout << "[" << i++ << "] "
					 << it.first->name << " (" << it.second << ") - "
					 << it.first->description << endl;
			 }
		}	 
		return this->weapons.size();
	}
	 
	//display armor
	int print_armor(bool label = false){
		unsigned int i = 1;
	 
		for(auto it : this->armor){
			if(label){
				cout << "[" << i++ << "] "
					 << it.first->name << " (" << it.second << ") - "
					 << it.first->description << endl;
			}
		}	 
		return this->armour.size();
	}
	 
	//display all items
	void print(bool label = false){
		if(this->items.size() == 0 &&
		   this->weapons.size() == 0 &&
		   this->armour.size() == 0){
			cout << "Nothing" << endl;
		}else{
			this->print_items(label);
			this->print_weapons(label);
			this->print_armor(label);
		}	 
		return;
	}

}; 
#endif