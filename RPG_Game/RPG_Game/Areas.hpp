#ifndef AREA_HPP
#define AREA_HPP
 
#include "inventory.hpp"
#include "creature.hpp"
#include "dialogue.hpp"
 
#include <vector>
 
class Area
{
private:
	Dialogue dialogue;
	Inventory items;
	vector<Creature*> creatures;
public:
 
    Area(){}
	
	//constructor
	Area(Dialogue dialogue, Inventory items, vector<Creature*> creatures){
		this->dialogue = dialogue;
		this->items = items;
		this->creatures = creatures;
	}

	//lists puts items in the area in their inventory
	void search(Creature& player){
		cout << "You find: " << endl;
	 	this->items.print();
		player.inventory.merge(&(this->items));
		this->items.clear();
	 	return;
	}


};
 
#endif
