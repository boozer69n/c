#ifndef CREATURE_HPP
#define CREATURE_HPP
 
#include <string>
#include <iostream>

using namespace std;
 
class Creature
{
private:
	//name and class (fighter, rogue)
	string name;
	string className;
 
	//creature stats
	int health;				 //current health (10-1000)
	int maxHealth;			 //max health (10-1000)
	int strength;            //damage (1-100)
	int endurance;           //determines max health (1-100)
	int dexterity;           //speed (1-100)
	double hitChance;		 //hit chance (1-150)
 	unsigned int level;		 //level (1-50)
 	unsigned int experience; //experience level

public:
	Creature(){}

    Creature(string name, int health, int strength, int endurance, int dexterity,
			 double hitChance, unsigned int level = 1, string className = ""){
		this->name = name;
		this->health = health;
		this->maxHealth = health;
		this->strength = strength;
		this->endurance = endurance;
		this->dexterity = dexterity;
		this->hitChance = hitChance;
		this->className = className;
		this->level = level;
		this->experience = 0;
    }

	void setHealth(int health){
		this->health = health);
	}
	int getHealth(){
		return this->health;
	}

	void setMaxHealth(int max){
		this->maxHealth = max;
	}
	int getMaxHealth(){
		return this->maxHealth;
	}

	void setStrength(int str){
		this->strength = str;
	}
	int getStrength(){
		return this->strength;
	}

	void setEndurance(int end){
		this->endurance = end;
	}
	int getEndurance(){
		return this->endurance;
	}

	void setDexterity(int dex){
		this->dexterity = dex;
	}
	int getDexterity(){
		return this->dexterity;
	}

	void setHitChance(double hit){
		this->hitChance = hit;
	}
	double getHitChance(){
		return this->hitChance;
	}

	void setLevel(unsigned int lvl){
		this->level = lvl;
	}
	unsigned int getLevel(){
		return this->level;
	}

	void setExperience(unsigned int exp){
		this->experience = exp;
	}
	unsigned int getExperience(){
		return this->experience;
	}

	//calculates the xp it takes for each level (scales as you level)
	unsigned int xpToLevel(unsigned int level){
		return 128 * level * level;
	}

	//check if level up required and adjusts level and stats
	bool levelUp(){
		//checks if next level is obtained
		if(this->experience >= xpToLevel(this->level+1)){
			++this->level;
 
			//keep track of stat increase for this level
			unsigned int healthBoost = 0;
			unsigned int strBoost = 0;
			unsigned int endBoost = 0;
			unsigned int dexBoost = 0;
 
			//every 3 levels get extra health
			if(this->level % 3 == 0){
				//give health proportional to current health
				healthBoost = 10 + (rand() % 4) + this->endurance / 4;
			}else{
				healthBoost = this->endurance / 4;
			}

			//increase str and end and 50% dex
			if(this->className == "Fighter"){
				strBoost = 1;
				endBoost = 1;
				if(rand() % 2 == 0) dexBoost = 1;
			}
			//increase end and dex and 50% str
			else if(this->className == "Rogue"){
				endBoost = 1;
				dexBoost = 1;
				if(rand() % 2 == 0) strBoost = 1;
			}
 
			//increase all stats
			this->maxHealth += healthBoost;
			this->strength += strBoost;
			this->endurance += endBoost;
			this->dexterity += dexBoost;
 
			//display stat increases
			cout << this->name << " is now level " << level << "!\n";
			cout << "Health +" << healthBoost << " -> " << this->maxHealth << endl;
			cout << "Strength +" << strBoost << " -> " << this->strength << endl;
			cout << "Endurance +" << endBoost << " -> " << this->endurance << endl;
			cout << "Dexterity +" << dexBoost << " -> " << this->dexterity<< endl;
			cout << "--------------------\n";
 			return true;
		}
		return false;
	}
}; 
#endif