#ifndef ATLAS_HPP
#define ATLAS_HPP
 
#include <vector>
 
#include "creature.hpp"
#include "item.hpp"
#include "weapon.hpp"
#include "armour.hpp"
#include "area.hpp"
 
// Atlas building functions. Atlases contain vectors of game data
// that is not modified in gameplay, so the base versions of
// creatures, items etc. Could easily be replaced with functions
// that load the information from config files instead of just
// defining the values in code
void buildatlas_creature(vector<Creature>& atlas);
void buildatlas_item(vector<Item>& atlas);
void buildatlas_weapon(vector<Weapon>& atlas);
void buildatlas_armor(vector<Armor>& atlas);
void buildatlas_area(vector<Area>& atlas,
					 vector<Item>& items,
					 vector<Weapon>& weapons,
					 vector<Armor>& armor,
					 vector<Creature>& creatures);
 
#endif