#ifndef WEAPON_HPP
#define WEAPON_HPP
 
#include "item.hpp"
#include <string>
 
// Weapons are items, so they should inherit their properties
class Weapon : public Item
{
private:
	unsigned damage; //1-50
	double hitRate; //5-30%
public:
	Weapon(string name, string description, int damage, double hitRate):
	  Item(name, description){
		this->damage = damage;
		this->hitRate = hitRate;
	}

	void setDamage(int damage){
		this->damage = damage;
	}
	int getDamage(){
		return this->damage;
	}

	void setHitRate(double hitRate){
		this->hitRate = hitRate;
	}
	double getHitRate(){
		return this->hitRate;
	}
}; 
#endif