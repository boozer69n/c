/*
http://www.penguinprogrammer.co.uk/rpg-tutorial-2/
*/

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <utility>
#include <cstdlib>
#include <ctime>

#include "Dialogue.hpp"
#include "Creature.hpp"
#include "Item.hpp"
#include "Armor.hpp"
#include "Weapon.hpp"
#include "Inventory.hpp"
#include "Areas.hpp"
#include "Atlas.hpp"
 
//declaring functions
Creature dialogue_newchar();

//creating vectors
vector<Creature> creatureAtlas;
vector<Item> itemAtlas;
vector<Weapon> weaponAtlas;
vector<Armor> armorAtlas;
vector<Area> areaAtlas;
 
//build the atlases
buildatlas_creature(creatureAtlas);
buildatlas_item(itemAtlas);
buildatlas_weapon(weaponAtlas);
buildatlas_armor(armorAtlas);
buildatlas_area(areaAtlas, itemAtlas, weaponAtlas, armorAtlas, creatureAtlas);


int main(void){
	srand(time(NULL));
	Creature player;

	//creates player
	int result = Dialogue("Welcome!", {"New Game"}).activate();
 	switch(result){
		case 1:
			player = dialogue_newchar();
			break;
		default:
			return 0;
			break;
	}

	while(true){
		if(player.getHealth() <= 0){
			cout << "Your Dead\nGame Over" << endl;
		}
	}
	
	// Set the current area to be the first area in the atlas,
	// essentially placing the player there upon game start
	Area* currentArea = &(areaAtlas[0]);
	 
	// Play the game until a function breaks the loop and closes it
	while(true)
	{
		//if player has died
		if(player.getHealth() <= 0){
			cout << "Your Dead\nGame Over" << endl;
			return 0;
		}
	 
		//activate the current area's dialogue
		result = currentArea->dialogue.activate();
	 
		//moving options
		if(currentArea == &(areaAtlas[0])){
			switch(result){
				//open the menu
				case 0:
					dialogue_menu(player);
					break;
				case 1:
					//move to area 1
					currentArea = &(areaAtlas[1]);
					break;
				case 2:
					//search the area
					currentArea->search(player);
					break;
				default:
					break;
			}
		}
		else if(currentArea == &(areaAtlas[1])){
			switch(result){
				//open the menu
				case 0:
					dialogue_menu(player);
					break;
				//move to area 0
				case 1:
					currentArea = &(areaAtlas[0]);
					break;
				//search the area
				case 2:
					currentArea->search(player);
					break;
				default:
					break;
			}
		}
	}	 
	return 0;
}

//new character
Creature dialogue_newchar(){
    string name;
    cout << "Choose your name: ";
    cin >> name;
 
	int result = Dialogue("Choose your class", {"Fighter", "Rogue"}).activate(); 
 
    switch(result){
        //creates a fighter
        case 1:
            return Creature(name, 35, 20, 10, 5, 10.0, 1, "Fighter");
            break;
        //creates a rogue
        case 2:
            return Creature(name, 30, 5, 10, 20, 15.0, 1, "Rogue");
            break;
         //default - should never happen
        default:
            return Creature(name, 30, 10, 10, 10, 10.0, 1, "Adventurer");
        break;
    }
}

