//#######################################################################################
//Student: Clinton Booze
//Professor: Victoria Heil
//Class: CSC215
//Week 1 - Assignment - 4_36
//2013-10-20
//Assignment: Write a program that separates a 4 digit number and performs a math function
//to encrypt and returns the encrypted number.
//Also the ability to decrypt a 4 digit number reversing the same math operation
//#######################################################################################

//used to display info to the user
#include <iostream>
//used to be able to use the core commands
using namespace std;

int main()
{
	//storing the four digit number
	int fourDigit = 0000;
	cout << "Please enter your 4 digit number: ";
	cin >> fourDigit;
	//separating the 4 digits to different variables
	int firstNum = ( fourDigit / 1000 ) % 10;
	int secondNum = ( fourDigit / 100 ) % 10;
	int thirdNum = ( fourDigit / 10 ) % 10;
	int fourthNum = ( fourDigit / 1 ) % 10;

	//prompt to ask whether to encrypt or decrypt
	int selectionED = 0;
	cout << "1 - Encrypt\n2 - Decrypt\nPlease pick a number: ";
	cin >> selectionED;
	
	//runs if 1 (encrypt) is input
	if ( selectionED == 1 )
		cout << ( thirdNum + 7 ) % 10
		<< ( fourthNum + 7 ) % 10
		<< ( firstNum + 7 ) % 10
		<< ( secondNum + 7 ) % 10
		<< endl;

	//runs if 2 (decrypt) is input
	if ( selectionED == 2 )
		cout << ( thirdNum + 3 ) % 10
		<< ( fourthNum + 3 ) % 10
		<< ( firstNum + 3 ) % 10
		<< ( secondNum + 3 ) % 10
		<< endl;

	//added to keep prompt up
	system("pause");
}