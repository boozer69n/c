/*
Assignment: Hash Table
Twitter Graph
The Twitter online social network consists of asymmetric relationships, which means that �Bob� may
follow �Alice�, but �Alice� may or may not follow �Bob�.  Implement a graph structure that will
mimic this type of relationship and include methods that will:

 - Compute the shortest path between two nodes on the network
 - Identify the cutoff point (nodes in which were removed, would separate the graph into multiple
   separate graphs).


Lab Report
In Word or a similar word processing program, write a Lab Report including the following sections:
  �    Introduction � in your own words, describe the structure and operations of linked lists.
  �    Programmer�s Guide  � Provide detailed documentation describing your implementation of the
                             graph.  Describe the structure of the graph and operation of each of
							 its member variables and methods. For each member function, give a
							 usage example. Provide a video and/or screen capture demoing your
							 project.
*/

#include "Graph.h"
#include <vector>
#include <iostream>

Graph twitters;
vector<Graph::node> line;
void fill();
void run_ui();
int get_person();

int main(){
	fill();
	run_ui();

	system("pause");	
}

//displays distance and the people it passes through
void run_ui(){
	line = twitters.min_dist(*twitters.get_node(get_person()),*twitters.get_node(get_person()));
	cout << "\nNumber of arcs: " << line.size() << endl;
	for (int i = line.size()-1; i >= 0; i--){
		cout << line[i].name << endl;
	}
}

//used to ask user from whom to whom
int get_person(){
	int choice;
	int num = 1;
	cout << num++ <<  " - Clinton" << endl;
	cout << num++ <<  " - Brooke" << endl;
	cout << num++ <<  " - Bob" << endl;
	cout << num++ <<  " - Ina" << endl;
	cout << num++ <<  " - Briana" << endl;
	cout << num++ <<  " - Amanda" << endl;
	cout << num++ <<  " - Gizmo" << endl;
	cout << num++ <<  " - Zoie" << endl;
	cout << num++ <<  " - Brandy" << endl;
	cout << num++ <<  " - Aiden" << endl;
	cout << num++ <<  " - Terry" << endl;
	cout << "Choice: ";

	cin >> choice;
	return choice;
}

//fills in the list with some people
void fill(){
	int clinton = twitters.new_user("Clinton");
	int brooke = twitters.new_user("Brooke");
	int bob = twitters.new_user("Bob");
	int ina = twitters.new_user("Ina");
	int briana = twitters.new_user("Briana");
	int amanda = twitters.new_user("Amanda");
	int gizmo = twitters.new_user("Gizmo");
	int zoie = twitters.new_user("Zoie");
	int brandy = twitters.new_user("Brandy");
	int aiden = twitters.new_user("Aiden");
	int terry = twitters.new_user("Terry");


	twitters.add_following(*twitters.get_node(terry),twitters.get_node(zoie));
	twitters.add_following(*twitters.get_node(clinton),twitters.get_node(terry));
	twitters.add_following(*twitters.get_node(bob),twitters.get_node(terry));
	twitters.add_following(*twitters.get_node(amanda),twitters.get_node(clinton));
	twitters.add_following(*twitters.get_node(amanda),twitters.get_node(brooke));
	twitters.add_following(*twitters.get_node(brooke),twitters.get_node(clinton));
	twitters.add_following(*twitters.get_node(briana),twitters.get_node(clinton));
	twitters.add_following(*twitters.get_node(ina),twitters.get_node(briana));
	twitters.add_following(*twitters.get_node(ina),twitters.get_node(bob));
	twitters.add_following(*twitters.get_node(aiden),twitters.get_node(bob));
	twitters.add_following(*twitters.get_node(brandy),twitters.get_node(ina));
	twitters.add_following(*twitters.get_node(brandy),twitters.get_node(aiden));
	twitters.add_following(*twitters.get_node(brooke),twitters.get_node(gizmo));
	twitters.add_following(*twitters.get_node(zoie),twitters.get_node(brandy));
	twitters.add_following(*twitters.get_node(zoie),twitters.get_node(amanda));
	twitters.add_following(*twitters.get_node(gizmo),twitters.get_node(amanda));
	twitters.add_following(*twitters.get_node(brandy),twitters.get_node(gizmo));
} //end fill