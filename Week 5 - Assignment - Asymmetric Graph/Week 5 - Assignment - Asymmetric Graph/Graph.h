
#ifndef GRAPH_H
#define GRAPH_H
#include <string>
#include <vector>
#include <list>
#include <limits.h>

using namespace std;

class Graph {

//node containing pointers and info
public:
struct node {
	string name;
	list<node*> following;
	list<node*> followers;
	int user_id;
	bool operator == (const node& right) const { return user_id == right.user_id; }
};

//private methods/variables
private:
	static int next_user_id;
	vector<node> twitter;
	int min_index(vector<int> dist, vector<node> unfinished);
	int get_vertex(node find);
	node new_node(string name);
	

//public methods/variables
public:
	Graph(void);
	virtual ~Graph(void);

	int new_user(string name);
	void add_following(node& person, node* follow);
	vector<node> min_dist(node start, node finish);
	node* get_node(int id);
	int get_size();
};

//header
//#####################################################
//cpp

//assigning usier id's
int Graph::next_user_id = 1;

//constructor
Graph::Graph(void){}

//creates a node with provided info and returns it
Graph::node Graph::new_node(string name){
	node* my_node = new node;
	my_node->name = name;
	my_node->user_id = Graph::next_user_id;
	Graph::next_user_id++;
	return *my_node;
} //end new_node

//pushes a node into the list as a new user
int Graph::new_user(string name){
	twitter.push_back(new_node(name));
	return Graph::next_user_id - 1;
} //end insert_node

//to start following a person
void Graph::add_following(node& person, node* follow){
	person.following.push_back(follow);
	follow->followers.push_back(&person);
} //end add_following

//returns a node with the coresponding it number
Graph::node* Graph::get_node(int id){
	for (int i = 0; i < twitter.size(); i++){
		if (twitter[i].user_id == id){
			return &(twitter[i]);
		}
	}
} //end get_node

//finding the minimum path and saving path to a vector to return
vector<Graph::node> Graph::min_dist(node start, node finish){
	vector<int> dist (twitter.size());
	vector<node*> prev (twitter.size());
	vector<node> finished;
	vector<node> unfinished (twitter);
	vector<node> shortest_path;
	int v1_index, v2_index;
	node v1;

	for (int i = 0; i < dist.size(); i++){
		dist[i] = INT_MAX;
		prev[i] = NULL;
	}

	dist[get_vertex(start)] = 0;
	
	while (finished.size() != twitter.size()){
		v1_index = min_index(dist, unfinished);
		v1 = unfinished[v1_index];
		unfinished[v1_index] = node();
		unfinished[v1_index].name = "NULL";
		if (v1 == finish)
			break;
		finished.push_back(v1);
		for (list<node*>::iterator it = v1.followers.begin(); it != v1.followers.end(); it++){
			v2_index = get_vertex(**it);
			if (dist[v1_index] + 1 < dist[v2_index]){
				dist[v2_index] = dist[v1_index] +1;
				prev[v2_index] = &twitter[v1_index];
			}
		}
	}

	node u = finish;
	while(!(u == start)){
		shortest_path.insert(shortest_path.begin(), u);
		u = *(prev[get_vertex(u)]);
	}
	shortest_path.insert(shortest_path.begin(), u);
	return shortest_path;
} //end min_dist

//returning the vertex of the passed in node
int Graph::get_vertex(node find){
	for (int j = 0; j < twitter.size(); j++){
		if (find == twitter[j]){
			return j;
		}
	}
} //end get_vertex

//returning the index of the minimum distance
int Graph::min_index(vector<int> dist, vector<node> unfinished){
	int distance = INT_MAX;
	int index = NULL;
	for (int k = 0; k < unfinished.size(); k++){
		if(dist[k] < distance && unfinished[k].name != "NULL"){
			distance = dist[k];
			index = k;
		}
	}
	return index;
} //end min_index

//returning the number of people on twitter
int Graph::get_size(){
	return twitter.size();
} //end get_size

//destructor
Graph::~Graph(void){}

#endif