#define NUMBER_OF_USERS 10 //defines numbers of users
#define NUMBER_OF_QUESTIONS 5 //defines number of questions
//############################################################################################################ 
#include <iostream> // to display to the user
#include <iomanip> //including iomanip functions - I added it so I could use the setw function
#include <string> //including string function
#include <array> //including array function
using namespace std; //to using std namespace
//############################################################################################################ 
int main() //program initialization
{
//############################################################################################################
	int answers[NUMBER_OF_USERS][NUMBER_OF_QUESTIONS]; //creates a multi-dimensional array for the responses
	string questions[NUMBER_OF_QUESTIONS]; //creates the string array for the questions
	questions[0] = "        Lowering Debt: "; //sets question 1
	questions[1] = " Gay/Lesbian Marriage: "; //sets question 2
	questions[2] = "       Lowering Taxes: "; //sets question 3
	questions[3] = "          Gun Control: "; //sets question 4
	questions[4] = "Government Monitoring: "; //sets question 5
//############################################################################################################ 
	for (int currentUser = 0; currentUser < NUMBER_OF_USERS; currentUser++) // for loop for the number of users
	{
		cout << "\nUSER " << currentUser + 1 << endl; //prints the user number
		for (int currentQuestion = 0; currentQuestion < NUMBER_OF_QUESTIONS; currentQuestion++)
		//for loop for number of questions
		{
			cout << "\nRate 1-10 based on importance, 10 being the highest\n" << questions[currentQuestion];
			//asking the questions
			cin >> answers[currentUser][currentQuestion]; //saving the answers to the answers array
		}
	}
//############################################################################################################
	int highLow[NUMBER_OF_QUESTIONS]; //creating an array to store the totals
	for (int zeroOut = 0; zeroOut < NUMBER_OF_QUESTIONS; zeroOut++) //for loop to zero out the values
	{
		highLow[zeroOut] = 0; //setting the values to 0 in the array
	}
//############################################################################################################
	for (int currentIssue = 0; currentIssue < NUMBER_OF_QUESTIONS; currentIssue++) //for loop to go through the questions
	{
		for (int totals = 0; totals < NUMBER_OF_USERS; totals++) //for loop to go through the votes to tally
		{
			int qTotal = answers[totals][currentIssue]; //saving the vote as qTotal
			highLow[currentIssue] += qTotal; //adding the qTotal vote to the highLow arrays tally
		}
	}
//############################################################################################################
	int impIssue = 0; //setting variable for impIssue = Important Issue
	int unimpIssue = 0; //setting variable for unimpIssue = Un-Important Issue
	int top = NUMBER_OF_USERS; //setting the variable top to 10 being the lowest number of votes
	int bot = (NUMBER_OF_USERS * 10); //setting the variable bot to 100 being the highest number of votes
//############################################################################################################
	for (int highest = 0; highest < (NUMBER_OF_QUESTIONS); highest++) //for loop going through the questions
	{
		if (top < highLow[highest]) //if statement comparing the totals
		{
			top = highLow[highest]; //if a new question has higher values the top will equal the new question
			impIssue = highest; //setting location of the highest value for later printing
		}
	}
//############################################################################################################
	for (int lowest = 0; lowest < (NUMBER_OF_QUESTIONS); lowest++) //for loop going through the questions
	{
		if (bot > highLow[lowest]) //if statement comparing the totals
		{
			bot = highLow[lowest]; //if a new question has lower values the bot will equal the new question
			unimpIssue = lowest; //setting location of the lowest value for later printing
		}
	}
//############################################################################################################
	cout << "\n" << "Users" << setw( 8 ); //printing the category of users for the first row
	for (int row = 0; row < NUMBER_OF_USERS; row++) //for loop running through the number of users
	{
		cout << row + 1 << setw( 3 ); //printing the user number for the top of the table
	}
	cout << "  Avg"; //printing avg for the column that contains the average

	for (int quest = 0; quest < NUMBER_OF_QUESTIONS; quest++) //for loop running through the number of questions
	{
		cout << "\n"; //blank line
		cout << "Question " << quest + 1 << setw( 3 ); //printing question then the number of the question
		for (int row = 0; row < NUMBER_OF_USERS; row++) //for loop running through the number of users
		{
			cout << answers[row][quest] << setw( 3 ); //printing the user responses one row at a time
		}
		cout << (highLow[quest] / NUMBER_OF_USERS); //printing the average number of votes
	}
//############################################################################################################
	cout << "\n";
	cout << "\n" << questions[impIssue] << " Is the most important issue"; //printing the important issue
	cout << "\n" << questions[unimpIssue] << " Is the least important issue"; //printing the least important issue
//############################################################################################################
	for (int quest = 0; quest < NUMBER_OF_QUESTIONS; quest++) //for loop running through the number of questions
	{
		cout << "\n"; //blank line
		cout << "\n" << "Question" << quest + 1 << " = " << questions[quest] << highLow[quest] << " votes";
		//explaining what question number is what question
	}
//############################################################################################################
	cout << "\n"; //blank line
	system("pause"); //pausing to keep window open
//############################################################################################################
}