#include "Substitutions.h"
#include <string>
using namespace std;

Substitutions::Substitutions(void)
{
}

double Substitutions::getOldAmount()
{
	return this->oldAmount;
}

void Substitutions::setOldAmount(double amount)
{
	this->oldAmount = amount;
}

double Substitutions::getNewAmount()
{
	return this->newAmount;
}

void Substitutions::setNewAmount(double amount)
{
	this->newAmount = amount;
}

string Substitutions::getOldMeasurement()
{
	return this->oldMeasurement;
}

void Substitutions::setOldMeasurement(string unit)
{
	this->oldMeasurement = unit;
}

string Substitutions::getNewMeasurement()
{
	return this->newMeasurement;
}

void Substitutions::setNewMeasurement(string unit)
{
	this->newMeasurement = unit;
}

string Substitutions::getOldItem()
{
	return this->oldItem;
}

void Substitutions::setOldItem(string item)
{
	this->oldItem = item;
}

string Substitutions::getNewItem()
{
	return this->newItem;
}

void Substitutions::setNewItem(string item)
{
	this->newItem = item;
}

unsigned int Substitutions::getBenefit()
{
	return this->benefit;
}

void Substitutions::setBenefit(unsigned int benefit)
{
	this->benefit = benefit;
}

void Substitutions::addBenefit(unsigned int newBenefit)
{
	this->benefit |= newBenefit;
}

bool Substitutions::isGlutenFree()
{
	return (this->benefit & GlutenFree) == GlutenFree;
}

bool Substitutions::isReducedCalorie()
{
	return (this->benefit & ReducedCalorie) == ReducedCalorie;
}

bool Substitutions::isReducedFat()
{
	return (this->benefit & ReducedFat) == ReducedFat;
}

bool Substitutions::isDiabeticFriendly()
{
	return (this->benefit & DiabeticFriendly) == DiabeticFriendly;
}

Substitutions::~Substitutions(void)
{
}