/*
Student: Clinton Booze
Professor: Jill Coddington
Week 5 - Final - Assessment
*/

#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <map>
#include <queue>
#include <vector>
#include "Substitutions.h"
using namespace std;

//functions
void addBenefit(const string &item, map<string, Substitutions> &subs);
void benList(BenefitType ben, const map<string, Substitutions> &subs);
void clearLine(istream& input);
double convert (const string &unit);
void fileUpdate(fstream &foodReplace, map<string, Substitutions> &subs);
string fullBenefit (int benefit);
void getIngredient(istringstream &stream, double &amount, string &unit, string &ingredient);
void itemAdd(map<string, Substitutions> &subs);
void mapList(fstream &foodReplace, map<string, Substitutions> &subs);
void mapList(map<string, Substitutions> &subs, string newLine);
BenefitType setBenefit();
void showList (const map<string, Substitutions> &subs);
void subSearch(string item, map<string, Substitutions> &subs);
template<class T> string queString(const T &input);

//main
int main()
{
	char uiSel;
	string chkIngredient, item;
	fstream foodReplace;
	map<string, Substitutions> subs;
	mapList(foodReplace, subs);

	//ui
	bool uiExit = false;
	while(!uiExit)
	{
		cout << "\nFood Substitutions" 
			 << "\nPlease Select One"
			 << "\n1 - See List of Current Ingredients"
			 << "\n2 - Search for an ingredients substitute"
			 << "\n3 - Add an Item to the list"
			 << "\n4 - List by Benefit"
			 << "\n5 - Add a Benefit to an existing item"
			 << "\n0 - Exit (Caution-File ONLY updates on Exit)"
			 << "\nSelection: ";
		cin.get(uiSel);
		clearLine(cin);
		switch(uiSel) {
			case '1':
				showList(subs);
				break;
			case '2':
				cout << "\nIngredient: ";
				getline(cin, chkIngredient); 
				subSearch(chkIngredient, subs);
				break;
			case '3':
				itemAdd(subs);
				clearLine(cin);
				break;
			case '4':
				benList(setBenefit(), subs);
				clearLine(cin);
				break;
			case '5':
				cout << "\nIngredient: ";
				getline(cin, item); 
				addBenefit(item, subs);
				clearLine(cin);
				break;
			case '0':
				uiExit = true;
				fileUpdate(foodReplace, subs);
				break;
			default:
				cout << "\nInvalid Input\n";
				break;
		}
		foodReplace.close();
	}
}

//searching for item and its replacement
void subSearch(string item, map<string, Substitutions> &subs)
{
	cout.precision(3);
	//convert string to lower case
	transform(item.begin(), item.end(), item.begin(), ::tolower);

	//declaring
	double oldC;
	double newC;
	Substitutions ingRep = subs[item];

	//warning
	cout << "\n#############################################################################";
	cout << "\n# Always consult your doctor before making significant changes to your diet #";
	cout << "\n#############################################################################";
	
	//displaying original item and amount
	cout << "\n\n-=Original Ingredient=-" << endl;
	cout << ingRep.getOldAmount() << " - " 
		 << ingRep.getOldMeasurement() << " - " 
		 << ingRep.getOldItem() << endl;

	//conversion display
	oldC = convert(ingRep.getOldMeasurement()) * ingRep.getOldAmount();
	cout << oldC << " (mL)" << endl;

	//displaying replacement item and amount
	cout << "\n-=Replacement Ingredient=-" << endl;
	cout << ingRep.getNewAmount() << " - " 
		 << ingRep.getNewMeasurement() << " - " 
		 << ingRep.getNewItem() << endl;

	//conversion display
	newC = convert(ingRep.getNewMeasurement()) * ingRep.getNewAmount();
	cout << newC << " (mL)" << endl;

	//displaying the benefit
	cout << "\n-=Benefit=-" << endl;
	cout << fullBenefit(ingRep.getBenefit()) << endl;
}

//function to add an item to the map
void itemAdd(map<string, Substitutions> &subs)
{
	//declaring variables
	double oAmnt;
	double nAmnt;
	string oUnit;
	string nUnit;
	string oItem;
	string nItem;
	string line = "";
	string benString;
	queue <string> qLine;

	//getting info for original ingredient
	cout << "\n-=Original Ingredient=-";
	cout << "\nExample: 1.25 (no fractions)";
	cout << "\nAmount: ";
	cin.clear();

	while(!(cin >> oAmnt))
	{
		cout << "\nInvalid\nAmount: ";
		clearLine(cin);
	}
	qLine.push(queString(oAmnt) + " ");
	cout << "\nAcceptable Measurements";
	cout << "\n(teaspoon, tablespoon, cup, ounce, gallon, pint)";
	cout << "\nMeasurement: ";
	cin >> oUnit;
	qLine.push(queString(oUnit) + " ");
	cout << "\nIngredient: ";
	cin.ignore();
	getline(cin, oItem);
	qLine.push(queString(oItem) + " | ");

	//getting info for replacement ingredient
	cout << "\n\n-=Replacement=-";
	cout << "\nExample: 1.25 (no fractions)";
	cout << "\nAmount: ";
	cin.clear();
	while(!(cin >> nAmnt))
	{
		cout << "\nInvalid\nAmount: ";
		clearLine(cin);
	}
	qLine.push(queString(nAmnt) + " ");
	cout << "\nAcceptable Measurements";
	cout << "\n(teaspoon, tablespoon, cup, ounce, gallon, pint)";
	cout << "\nMeasurement: ";
	cin >> nUnit;
	qLine.push(queString(nUnit) + " ");
	cout << "\nIngredient: ";
	cin.ignore();
	getline(cin, nItem);
	qLine.push(queString(nItem) + " | ");

	//benString = setBenefit();
	qLine.push(queString(setBenefit()) + "\n");

	//creating string in the proper format for txt file
	while(!qLine.empty())
	{
		string temp = qLine.front();
		qLine.pop();
		line += temp;
	}

	//convert string to lower case
	transform(line.begin(), line.end(), line.begin(), ::tolower);

	//adding new item to the map
	mapList(subs, line);
}

//pulling in the info from the file and mapping it
void mapList(fstream &foodReplace, map<string, Substitutions> &subs) 
{
	//opens the file
	foodReplace.open("foodReplace.txt", ios::in);
	if(!foodReplace)
	{
		cerr << "File could not be opened" << endl;
		exit (EXIT_FAILURE);
	}

	string line;
	while (getline (foodReplace, line)) //gets the line and saves it to line
	{
		Substitutions item;
		istringstream readLine(line); //reads it into readLine

		//declaring variables
		double amount;
		string unit;
		string ingredient;
		unsigned int benefit;

		//gets old ingredient and saves in object
		getIngredient(readLine, amount, unit, ingredient);
		item.setOldAmount(amount);
		item.setOldMeasurement(unit);
		item.setOldItem(ingredient);

		//gets new ingredient and saves to object
		getIngredient(readLine, amount, unit, ingredient);
		item.setNewAmount(amount);
		item.setNewMeasurement(unit);
		item.setNewItem(ingredient);

		//reads in last piece and saves in object
		readLine >> benefit;
		item.setBenefit(benefit);

		//inserts object into map
		subs.insert(pair<string, Substitutions>(item.getOldItem(), item));
	}
	//closes file
	foodReplace.close();
}

//overloaded to accept a string for the adding ingredient function
void mapList(map<string, Substitutions> &subs, string newLine)
{
	Substitutions item;
	istringstream readLine(newLine); //reads it into readLine

	//declaring variables
	double amount;
	string unit;
	string ingredient;
	unsigned int benefit;

	//gets old ingredient and saves in object
	getIngredient(readLine, amount, unit, ingredient);
	item.setOldAmount(amount);
	item.setOldMeasurement(unit);
	item.setOldItem(ingredient);

	//gets new ingredient and saves to object
	getIngredient(readLine, amount, unit, ingredient);
	item.setNewAmount(amount);
	item.setNewMeasurement(unit);
	item.setNewItem(ingredient);

	//reads in last piece and saves in object
	readLine >> benefit;
	item.setBenefit(benefit);

	//inserts object into map
	subs.insert(pair<string, Substitutions>(item.getOldItem(), item));
}

//function to extract amount-units-ingredient from the stream pulling from the file
void getIngredient(istringstream &stream, double &amount, string &unit, string &ingredient)
{
	//resetting variables
	amount = 0;
	unit = "";
	ingredient = "";
	string temp;

	//setting variables
	stream >> amount;
	stream >> unit;
	stream >> temp;

	//read until delimiter is hit
	while (temp != "|")
	{
		ingredient += temp + " ";
		stream >> temp;
	}

	//removes the space at the end of the ingredient
	ingredient = ingredient.substr(0, ingredient.length() - 1);
}

//function to display the list of ingredients
void showList (const map<string, Substitutions> &subs)
{
	cout << "\nCurrent List of Ingredients" << endl;
	//pair equals an single item from the map
	for_each(subs.begin(), subs.end(), [] (const pair<string, Substitutions> &item)
	{
		cout << item.first << endl;
	});
	cout << '\n' << endl;
}

//function to do conversions
double convert (const string &unit)
{
	//default value
	double conv = 1;
	
	//setting conversion amount to mL
	if(unit == "teaspoon")
		conv = 5;
	if(unit == "tablespoon")
		conv = 15;
	if(unit == "ounce")
		conv = 30;
	if(unit == "cup")
		conv = 240;
	if(unit == "gallon")
		conv = 3800;
	if(unit == "pint")
		conv = 500;
	return conv;
}

//returning the benefit
string fullBenefit (int benefit)
{
	//returning lengthened format of benefit
	string ben = "";
	if (benefit & GlutenFree) { ben += "gluten free "; }
	if (benefit & ReducedCalorie) { ben += "reduced calorie "; }
	if (benefit & ReducedFat) { ben += "reduced fat "; }
	if (benefit & DiabeticFriendly) { ben += "diabetic friendly "; }
	return ben;
}

//clear input
void clearLine(istream& input)
{
	char clr;
	while (input.get(clr) && clr != '\n');
}

//taking input and converting to string
template<class T> string queString(const T &input)
{
	//will catch if anything that cant be converted to a string is passed into it
	ostringstream convert;
	try
	{
		convert << input;
		return convert.str();
	}
	catch(exception)
	{
		cerr << "DOH!";
	}

	return "";
}

//list the ones that are of a specific benefit
void benList(BenefitType ben, const map<string, Substitutions> &subs)
{
	cout << "\nBenefit: " << fullBenefit(ben) << endl;
	for_each(subs.begin(), subs.end(), [ben] (pair<string, Substitutions> mItem)
	{
		if(mItem.second.getBenefit() & ben)
		{
			cout << mItem.first << endl;
		}
	});
}

//add an additional benefit to an item, meaning it can have more than 1 benefit
//and will display under both types when under benList
void addBenefit(const string &item, map<string, Substitutions> &subs)
{
	subs[item].addBenefit(setBenefit());
}

//sets the proper number to indicate the benefit(s)
BenefitType setBenefit()
{
	int ben = 0;
	while(true)
	{
		//options for the benefit
		cout << "\n\nPlease choose a benefit"
		     << "\n1 - Gluten Free"
			 << "\n2 - Reduced Calorie"
			 << "\n3 - Reduced Fat"
			 << "\n4 - Diabetic Friendly"
			 << "\nSelection: ";
		while(!(cin >> ben))
		{
			cout << "\nInvalid Input\n";
			clearLine(cin);
			cin >> ben;
		}

		//returning the correct benefit number
		if(ben == 1) { return BenefitType::GlutenFree;}
		if(ben == 2) { return BenefitType::ReducedCalorie;}
		if(ben == 3) { return BenefitType::ReducedFat;}
		if(ben == 4) { return BenefitType::DiabeticFriendly;}
		cout << "\nInvalid Input\n";
		clearLine(cin);
	}
}

//repopulating text file once program exits
void fileUpdate(fstream &foodReplace, map<string, Substitutions> &subs)
{
	//declaring variables
	string line = "";
	queue <string> qLine;

	//opening file for appending
	foodReplace.open("foodReplace.txt", ios::out);
	//in case the file cant be opened/modified
	if(!foodReplace)
	{
		cerr << "File could not be opened" << endl;
		exit (EXIT_FAILURE);
	}

	//using lambda to itterate through the map
	for_each(subs.begin(), subs.end(), [&qLine] (pair<string, Substitutions> mItem)
	{
		qLine.push(queString(mItem.second.getOldAmount()) + " ");
		qLine.push(queString(mItem.second.getOldMeasurement()) + " ");
		qLine.push(queString(mItem.second.getOldItem()) + " | ");
		qLine.push(queString(mItem.second.getNewAmount()) + " ");
		qLine.push(queString(mItem.second.getNewMeasurement()) + " ");
		qLine.push(queString(mItem.second.getNewItem()) + " | ");
		qLine.push(queString(mItem.second.getBenefit()) + "\n");
	});

	//creating string in the proper format for txt file
	while(!qLine.empty())
	{
		string temp = qLine.front();
		qLine.pop();
		line += temp;
	}

	//inputting string
	foodReplace << line;

	//closing file
	foodReplace.close();
}