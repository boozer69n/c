#ifndef SUBSTITUTIONS_H
#define SUBSTITUTIONS_H
#include <string>
using namespace std;

enum BenefitType
{
	GlutenFree =	    1 << 0,   // 0001 (1)
	ReducedCalorie =    1 << 1,   // 0010 (2)
	ReducedFat =		1 << 2,	  // 0100 (4)
	DiabeticFriendly =  1 << 3,   // 1000 (8)
};

class Substitutions
{
public:
	Substitutions(void);
	virtual ~Substitutions(void);

	//getters and setters
	double getOldAmount();
	void setOldAmount(double);
	double getNewAmount();
	void setNewAmount(double);
	string getOldMeasurement();
	void setOldMeasurement(string);
	string getNewMeasurement();
	void setNewMeasurement(string);
	string getOldItem();
	void setOldItem(string);
	string getNewItem();
	void setNewItem(string);
	unsigned int getBenefit();
	bool isGlutenFree();
	bool isReducedCalorie();
	bool isReducedFat();
	bool isDiabeticFriendly();
	void setBenefit(unsigned int);
	void addBenefit(unsigned int);

private:
	//saving each info for ingredients and replacements
	double oldAmount;
	double newAmount;
	string oldMeasurement;
	string newMeasurement;
	string oldItem;
	string newItem;
	unsigned int benefit;
};

#endif