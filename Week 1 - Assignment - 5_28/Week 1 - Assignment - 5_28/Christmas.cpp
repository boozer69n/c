//#######################################################################################
//Student: Clinton Booze
//Professor: Victoria Heil
//Class: CSC215
//Week 1 - Assignment - 5_28
//2013-10-20
//Assignment: Write a program that writes out the 12 days of Christmas with 2 switches
//and a repetition function.
//#######################################################################################

//used to display info to the user
#include <iostream>
//used to be able to use the core commands
using namespace std;

//initializing program
int main()
{
	//defining dayInput as 0 and asking user to set the day they want to start the song at
	int dayInput = 0;
	cout << "Which day would you like to start on?\nDay 1 - 12: ";
	cin >> dayInput;
	//beginning for loop at 1 and adding 1 each time through
	for (dayInput; dayInput <= 12; ++dayInput)
	{
		//switch statement defining the initial part of the chorus
		switch (dayInput)
		{
		case 1:
			cout << "\nOn the first day of Christmas\nmy true love sent to me:\n";
			break;
		case 2:
			cout << "\nOn the second day of Christmas\nmy true love sent to me:\n";
			break;
		case 3:
			cout << "\nOn the third day of Christmas\nmy true love sent to me:\n";
			break;
		case 4:
			cout << "\nOn the fourth day of Christmas\nmy true love sent to me:\n";
			break;
		case 5:
			cout << "\nOn the fifth day of Christmas\nmy true love sent to me:\n";
			break;
		case 6:
			cout << "\nOn the sixth day of Christmas\nmy true love sent to me:\n";
			break;
		case 7:
			cout << "\nOn the seventh day of Christmas\nmy true love sent to me:\n";
			break;
		case 8:
			cout << "\nOn the eighth day of Christmas\nmy true love sent to me:\n";
			break;
		case 9:
			cout << "\nOn the ninth day of Christmas\nmy true love sent to me:\n";
			break;
		case 10:
			cout << "\nOn the tenth day of Christmas\nmy true love sent to me:\n";
			break;
		case 11:
			cout << "\nOn the eleventh day of Christmas\nmy true love sent to me:\n";
			break;
		case 12:
			cout << "\nOn the twelfth day of Christmas\nmy true love sent to me:\n";
			break;
		}
		
		//switch statement naming what was given on each day
		switch (dayInput)
		{
			case 12:
				cout << "12 Drummers Drumming\n";
			case 11:
				cout << "Eleven Pipers Piping\n";
			case 10:
				cout << "Ten Lords a Leaping\n";
			case 9:
				cout << "Nine Ladies Dancing\n";
			case 8:
				cout << "Eight Maids a Milking\n";
			case 7:
				cout << "Seven Swans a Swimming\n";
			case 6:
				cout << "Six Geese a Laying\n";
			case 5:
				cout << "Five Golden Rings\n";
			case 4:
				cout << "Four Calling Birds\n";
			case 3:
				cout << "Three French Hens\n";
			case 2:
				cout << "Two Turtle Doves\nAnd ";
			case 1:
				cout << "A Partridge in a Pear Tree\n";

		}
	}
	//puts an empty line and pauses the cmd prompt so you can read what was done
	cout << "\n";
	system("pause");
}