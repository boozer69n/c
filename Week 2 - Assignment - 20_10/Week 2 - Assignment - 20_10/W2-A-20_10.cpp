/*
Student: Clinton Booze
Professor: Jill Coddington
Week 2 - Assignment - 20.10
//#################################################################################################
(Quicksort)
The recursive sorting technique called quicksort uses the following basic algorithm
for a one-dimensional vector of values:
	a) Partitioning Step: Take the first element of the unsorted vector and determine its final
	location in the sorted vector (i.e., all values to the left of the element in the vector are
	less than the element�s value, and all values to the right of the element in the vector are
	greater than the element�s value�we show how to do this below). We now have one
	value in its proper location and two unsorted subvectors.

	b) Recursion Step: Perform the Partitioning Step on each unsorted subvector.
	
	Each time the Partitioning Step is performed on a subvector, another value is placed in
its final location of the sorted vector, and two unsorted subvectors are created. When a
subvector consists of one element, that element�s value is in its final location (because a
one-element vector is already sorted).

	The basic algorithm seems simple enough, but how do we determine the final position
of the first element value of each subvector? As an example, consider the following
set of values (the value in bold is for the partitioning element�it will be placed in its
final location in the sorted vector):
						37 2 6 4 89 8 10 12 68 45
	Starting from the rightmost element of the vector, compare each element value with 37
until an element value less than 37 is found; then swap 37 and that element�s value.
The first element value less than 37 is 12, so 37 and 12 are swapped. The new vector is
						12 2 6 4 89 8 10 37 68 45
	Element value 12 is in italics to indicate that it was just swapped with 37.
Starting from the left of the vector, but beginning with the element value after 12,
compare each element value with 37 until an element value greater than 37 is found�
then swap 37 and that element value. The first element value greater than 37 is 89, so
37 and 89 are swapped. The new vector is
						12 2 6 4 37 8 10 89 68 45
	Starting from the right, but beginning with the element value before 89, compare each
element value with 37 until an element value less than 37 is found�then swap 37 and
that element value. The first element value less than 37 is 10, so 37 and 10 are
swapped. The new vector is
						12 2 6 4 10 8 37 89 68 45
	Starting from the left, but beginning with the element value after 10, compare each
element value with 37 until an element value greater than 37 is found�then swap 37
and that element value. There are no more element values greater than 37, so when we
compare 37 with itself, we know that 37 has been placed in its final location of the
sorted vector. Every value to the left of 37 is smaller than it, and every value to the
right of 37 is larger than it.
	Once the partition has been applied on the previous vector, there are two unsorted
subvectors. The subvector with values less than 37 contains 12, 2, 6, 4, 10 and 8. The
subvector with values greater than 37 contains 89, 68 and 45. The sort continues
recursively, with both subvectors being partitioned in the same manner as the original
vector.
	Based on the preceding discussion, write recursive function quickSortHelper to
sort a one-dimensional integer vector. The function should receive as arguments a
starting index and an ending index on the original vector being sorted.
*/

#include <iostream>
#include <array>
#include <string>
#include <stdlib.h>
#include <time.h>
using namespace std;

#define aSize 20

//declaring functions
void flip(int &pos1, int &pos2); //flipping numbers
int partition(int* splitArray, int pivot, int begPos, int endPos); //locating pivot point
void printArray(int* pArray, int pos1, int pos2); //displaying the array
void quickSort(int* sortArray, int begPos, int endPos); //breaking up the array for sort

int flips = 0;

int main()
{
	//declaring variables
	int originalArray[aSize];
	int i;
	int rNum;

	//seed for random numbers
	srand (time(NULL));

	//filling array with random numbers
	for(i = 0; i < aSize; i++)
	{
		rNum = rand() % 100 + 1;
		originalArray[i] = rNum;
	}

	//displaying original array
	cout << "Original Array" << endl;
	printArray(originalArray, 0, aSize);
	cout << '\n';

	//begin the sorting process
	quickSort(originalArray, 0, aSize - 1);

	//displaying the sorted array
	cout << "\nSorted Array" << endl;
	printArray(originalArray, 0, aSize);
	cout << "\n\n";

	//displaying the number of element flips
	cout << "Number of Flips: " << flips << '\n';
	system("pause");
}

//locating the pivot point
int partition(int* splitArray, int pivot, int begPos, int endPos)
{
	int first;
	int last;

	first = begPos;
	last = endPos;

	//checking size is larger than 1
	while(first < last)
	{
		//moves location left if the element next to it is more
		//or if it hits the end
		while(pivot < splitArray[last] && last > first)
		{
			last--;
		}
		//flips elements
		flip(splitArray[first], splitArray[last]);
		
		//moves location right if the element next to it is more or the same
		//or if it hits the boundary
		while(pivot >= splitArray[first] && first < last)
		{
			first++;
		}
		//flips elements
		flip(splitArray[last], splitArray[first]);
	}
	//returns the pivot point
	return first;
}

//function going through breaking down the array per the pivot point
void quickSort(int* sortArray, int begPos, int endPos)
{
	int pivot;
	int splitPoint;

	pivot = sortArray[begPos];

	//checking if there is only 1 int
	if(endPos > begPos)
	{
		//sends to function to get the split point
		splitPoint = partition(sortArray, pivot, begPos, endPos);

		//recursive running it through to break it down
		quickSort(sortArray, begPos, splitPoint-1); //sort pre partition (sub-array)
		quickSort(sortArray, splitPoint+1, endPos); //sort post partition (sub-array)
	}
}

//function to change the position of the elements in the array
//passing in by reference to modify the array
void flip(int &pos1, int &pos2)
{
	int i; //temp
	i = pos1; //storing pos1
	pos1 = pos2; //saving pos2 in pos1
	pos2 = i; //putting temp into pos2
	flips++;
}

//function to display the elements in the array through looping
void printArray(int* pArray, int pos1, int pos2)
{
	int i;
	for( i = pos1; i < pos2; i++)
		cout << pArray[i] << ' ';
}