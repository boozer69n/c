/*
Author: Clinton Booze
Professor: Jason Turner
Week 3 - Assignment - Binary Search Tree
###################################################################################################
Assignment
Binary Search Tree
##Implementation
In this lab, you will implement a binary search tree data structure in C++, and turn in a program
evaluating the data structure.
Implement a templated Binary Search Tree data structure, implementing the following functionality:
  �    Add (concatenate node, resulting in balanced tree)
  �    Delete (delete given node, restructure sub-tree)
  �    Minimum (Find minimum key)
  �    Maximum(Find Maximum Key)
  �    Search (Given Key, return node with equal key)
  �    Sort(return a list of sorted nodes)
To evaluate this data structure, write a program that reads a data file (with the filename passed
as a command line parameter) and populates the data structure.

##Lab Report
In Word or a similar word processing program, write a Lab Report including the following sections:
  �    Introduction � in your own words, describe the structure and operations of linked lists.
  �    Programmer�s Guide  � Provide detailed documentation describing your implementation of
	   binary search tree.  Describe the structure of the tree and describe the meaning and
	   operation of each of its member variables and methods. For each member function,
	   give a usage example.  Provide a video and/or screen capture demoing your project.
###################################################################################################
*/
#ifndef BST_H
#define BST_H
#include <vector>
#include <stack>

using namespace std;

template <class T> class BST
{

//node containing pointers and info
struct BST_Node {
	T node_key;
	BST_Node *node_left;
	BST_Node *node_right;
	BST_Node *parent;
};

//private methods/variables
private:
	BST_Node *root;
	vector<T> vector_sort;

	void tree_sort(BST_Node* node);
	BST_Node *insert_node(BST_Node **node,T key, BST_Node* parent);
	BST_Node *tree_search(BST_Node *node, T key);
	BST_Node *get_min_node(BST_Node *node);
	BST_Node *get_max_node(BST_Node *node);
	bool delete_node(BST_Node **node);

//public methods/variables
public:
	BST(void);
	virtual ~BST(void);

	BST_Node *new_node(T key, BST_Node* parent);
	BST_Node *insert_node(T key);
	BST_Node *tree_search(T key);
	T get_min();
	T get_max();
	vector<T> tree_sort();
	bool delete_node(T key);
};

//header
//#####################################################
//cpp

//constructor
template <class T> BST<T>::BST(void){
	root = NULL;
}

//creates a node with provided info and returns it
template <class T> typename BST<T>::BST_Node * BST<T>::new_node(T key, BST_Node* parent){
	BST_Node *created_node = new BST_Node();
	created_node->node_key = key;
	created_node->node_left = NULL;
	created_node->node_right = NULL;
	created_node->parent = parent;
	return created_node;
} //end new_node

//runs down the left side to get the minimum node
template <class T> typename BST<T>::BST_Node* BST<T>::get_min_node(BST_Node *node){
	BST_Node *temp = node;
	while(temp->node_left != NULL){
		temp = temp->node_left;
	}
	return temp;
} //end get_min_node

//overloaded so user doesnt have to provide a node
template <class T> T BST<T>::get_min(){
	return get_min_node(root)->node_key;
} //end get_min

//overloaded so user doesnt have to provide a node
template<class T> T BST<T>::get_max(){
	return get_max_node(root)->node_key;
} //end get_max

//runs down the right side to get the maximum node
template <class T> typename BST<T>::BST_Node* BST<T>::get_max_node(BST_Node *node){
	BST_Node *temp = node;
	while(temp->node_right != NULL){
		temp = temp->node_right;
	}
	return temp;
} //end get_max_node

//creates a node and locates its correct spot within the tree
template <class T> typename BST<T>::BST_Node * BST<T>::insert_node(BST_Node **node,T key, BST_Node* parent){
	if(*node == NULL){ //if empty make it
		*node = this->new_node(key, parent);
	}
	else if(key <= (*node)->node_key){ //if less than or equal to go left
		this->insert_node(&((*node)->node_left), key, *node);
	}else{ //if greater than goto right
		this->insert_node(&((*node)->node_right), key, *node);
	}
	return *node;
} //end insert_node

//overloaded so user doesnt have to provide a node
template <class T> typename BST<T>::BST_Node * BST<T>::insert_node(T key){
	return this->insert_node(&root, key, NULL);
} //end insert_node

//using recursion to find the required node
template <class T> typename BST<T>::BST_Node * BST<T>::tree_search(BST_Node *node, T key){
	if(node == NULL){
		return 0;
	}
	else if (node->node_key == key){
		return node;
	}
	else if (key <= node->node_key){
		return this->tree_search(node->node_left, key);
	}else{
		return this->tree_search(node->node_right, key);
	}
} //end tree_search

//overloaded so user doesnt have to provide a node
template <class T> typename BST<T>::BST_Node * BST<T>::tree_search(T key){
	return this->tree_search(this->root, key);
} //end tree_search

//using recursion to work down and back up within the tree to place within vector in order
template <class T> void BST<T>::tree_sort(BST_Node *node){
	if(node == NULL){ return; } //if node empty

	this->tree_sort(node->node_left);
	this->vector_sort.push_back(node->node_key);
	this->tree_sort(node->node_right);
} //end tree_sort

//overloaded so user doesnt have to provide a node
template <class T> vector<T> BST<T>::tree_sort(){
	this->vector_sort.clear();
	this->tree_sort(this->root);
	return this->vector_sort;
} //end tree_sort

//delete a node and update tree structure to accomidate the deletion
template <class T> bool BST<T>::delete_node(BST_Node **node){

	//if the node has no children
	if((*node)->node_left == NULL && (*node)->node_right == NULL){
		if(((*node)->parent)->node_left == *node){
			((*node)->parent)->node_left = NULL;
		}else{
			((*node)->parent)->node_right = NULL;
		}

		delete *node;
		*node = NULL;
		return true;
	} //end no children

	//if right child only
	if((*node)->node_left == NULL){
		if(((*node)->parent)->node_left == *node){
			((*node)->parent)->node_left = (*node)->node_right;
		}else{
			((*node)->parent)->node_right = (*node)->node_right;
		}

		((*node)->node_right)->parent = (*node)->parent;
		delete *node;
		return true;
	} //end right child only

	//if left child only
	if((*node)->node_right == NULL){
		if(((*node)->parent)->node_left == *node){
			((*node)->parent)->node_left = (*node)->node_left;
		}else{
			((*node)->parent)->node_right = (*node)->node_left;
		}

		((*node)->node_left)->parent = (*node)->parent;

		delete *node;
		return true;
	} //end left child only

	//if 2 children
	else{
		BST_Node *temp = get_max_node((*node)->node_left);
		(*node)->node_key = temp->node_key;
		delete_node(&temp);
	} //end 2 children
} //end delete_node

//overloaded so user doesnt have to provide a node
template <class T> bool BST<T>::delete_node(T key){
	BST_Node* temp = tree_search(key);
	return this->delete_node(&temp);
} //end delete_node

//destructor
template <class T> BST<T>::~BST(void){}

#endif