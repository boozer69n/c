#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "BST.h"
#include "Hash.h"
#include <string>

using namespace std;

BST<int> my_tree;
Hash<int> my_hash;
fstream stream;

void fill_tree(fstream &stream);
void fill_file(fstream &stream);

void bst_test();
void hash_test();

int main(){
	bst_test();
	hash_test();
}

//tests to be run on the BST
void bst_test(){
	vector<int> my_vector;

	fill_file(stream);
	fill_tree(stream);

	cout << "\n#####Binary Search Tree Assignment#####" << endl;
	cout << "\nSorted Tree:" << endl;

	my_vector = my_tree.tree_sort();
	for (unsigned int i = 0; i < my_vector.size(); i++){
		cout << my_vector[i] << " ";
	}
	cout << endl;

	cout << "\n\nMinimum: " << my_tree.get_min() << endl;
	cout << "Maximum: " << my_tree.get_max() << endl;

	cout << "\nDeletion Test: Deleting 50 random Nodes" << endl;
	for (int i = 25; i < 75; i++){
		my_tree.delete_node(my_vector[i]);
	}

	cout << "Remaining Nodes:" << endl;
	my_vector = my_tree.tree_sort();
	for (unsigned int i = 0; i < my_vector.size(); i++){
		cout << my_vector[i] << " ";
	}

	system("pause");
} //end bst_test

//pulls ints from the file and populates the BST
void fill_tree(fstream &stream){
	stream.open("random_numbers.txt", ios::in);
	if(!stream){
		cerr << "File could not be opened" << endl;
		exit (EXIT_FAILURE);
	}

	int temp;
	if(stream.is_open()){
		while(!stream.eof()){
			stream >> temp;
			my_tree.insert_node(temp);
		}
	}
	stream.close();
} //end fill_tree

//generated 100 random numbers and fills a file
void fill_file(fstream &stream){
	stream.open("random_numbers.txt", ios::out);
	if(!stream){
		cerr << "File could not be opened" << endl;
		exit (EXIT_FAILURE);
	}
	srand(time(NULL));
	int temp;

	if(stream.is_open()){
		for (int i = 0; i < 100; i++){
			temp = rand() % 100 + 1;
			stream << temp;
			if (i < 99){
				stream << endl;
			}
		}
	}
	stream.close();
} //end fill_file

//test to be run on the hash table
void hash_test(){
	//filling table with information
	string key = "apple";
	my_hash.insert_node(key, 5);
	key = "pineapple";
	my_hash.insert_node(key, 9);
	key = "tomato";
	my_hash.insert_node(key, 3);
	key = "spagetti";
	my_hash.insert_node(key, 4);
	key = "bananna";
	my_hash.insert_node(key, 6);
	key = "steak";
	my_hash.insert_node(key, 12);
	key = "peas";
	my_hash.insert_node(key, 46);
	key = "grapes";
	my_hash.insert_node(key, 94);
	key = "oranges";
	my_hash.insert_node(key, 69);
	key = "gum";
	my_hash.insert_node(key, 24);
	key = "beans";
	my_hash.insert_node(key, 675);

	cout << "\n\n#####Hash Table Assignment#####" << endl;

	cout << "Searching Key: grapes" << endl;
	cout << "Found Key: " << (my_hash.search_node("grapes"))->key << endl;
	cout << "Found Info: " << (my_hash.search_node("grapes"))->info << endl;

	cout << "\n\nPrinting Hash Table" << endl;
	my_hash.print_hash();

	system("pause");
} //end hash_test