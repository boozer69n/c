/*
Author: Clinton Booze
Professor: Jason Turner
Week 3 - Assignment - Hash Table
###################################################################################################
Assignment
Hashing
##Implementation
In this lab, you will implement a hash table that will be capable of the following:
  �    adding data
  �    finding data
  �    printing all items
  �    searching
  �    efficiently handles collisions (method of your choice)

##Lab Report
In Word or a similar word processing program, write a Lab Report including the following sections:
  �    Introduction � in your own words, describe the structure and operations of linked lists.
  �    Programmer�s Guide  � Provide detailed documentation describing your implementation of hash
	   table. Describe the structure of the table, the methods, and how collisions are handled.
	   For each member function, give a usage example. Provide a video and/or screen capture
	   demoing your project.
###################################################################################################
*/
#ifndef HASH_H
#define HASH_H
#include <list>
#include <algorithm>
#include <string>
using namespace std;

//table size
const int array_size = 27;

template <class T> class Hash
{

//node and containing info
struct hash_node{
	string key;
	T info;
};

//private methods/variables
private:
	list<hash_node> hash_array[array_size];
	int hash_map(string key);
	hash_node *create_node(string key, T info);

//public methods/variables
public:
	Hash(void);
	virtual ~Hash(void);

	void insert_node(string key, T info);
	hash_node *search_node(string key);
	void print_hash();
};

//header
//#####################################################
//cpp

//constructor
template <class T> Hash<T>::Hash(void){}

//create a node with key and info to return
template <class T> typename Hash<T>::hash_node * Hash<T>::create_node(string key, T info){
	hash_node *new_node = new hash_node;
	new_node->key = key;
	new_node->info = info;
	return new_node;
} //end create_node

//equation to return the location that the node should be stored
template <class T> int Hash<T>::hash_map(string key){
	char first_letter = tolower(key[0]);
	int temp = (int)first_letter;
	temp = temp - 96;
	if (temp > array_size){
		return 0;
	}else{
		return temp;
	}
} //end hash_map

//take node and insert into correct linked list
template <class T> void Hash<T>::insert_node(string key, T info){
	hash_array[this->hash_map(key)].push_front(*(create_node(key, info)));
} //end insert_node

//use provided key to search through the tree for correct node to return
template <class T> typename Hash<T>::hash_node * Hash<T>::search_node(string key){
	list<hash_node> *search_list = &(hash_array[this->hash_map(key)]);
	list<hash_node>::iterator it;

	for (it = search_list->begin(); it != search_list->end(); ++it){
		if((*it).key == key){
			return &(*it);
		}
	}
	return NULL;
} //end search_node

//will step though the array printing each linked list
template <class T> void Hash<T>::print_hash(){
	for (int i = 0; i < array_size; i++){
		list<hash_node> temp = hash_array[i];
		list<hash_node>::iterator it;

		cout << "Position: " << i << endl;
		for (it = temp.begin(); it != temp.end(); ++it){
			cout << "Key: " << (*it).key << endl;
			cout << "Info: " << (*it).info << endl;
		}
		cout << "-----------------------" << endl;
	}
} //end print_hash

//destructor
template <class T> Hash<T>::~Hash(void){}

#endif