#include <iostream> // to display to the user
using namespace std; //to use std namespace

int main()
{

int myAge = 34; //setting the variable myAge to 34

int* ageAddress = &myAge;
//couple things are going on here
//first: the &myAge is giving the memory location of the variable myAge
//second: the int* is pointing to the &myAge and saving it to the variable ageAddress

cout << "Address: " << &myAge << endl; //displaying the address of the variable &myAge
cout << "Value: " << myAge << endl; //displaying the value of myAge which is 34
cout << "Address: " << ageAddress << endl; //displaying the variable ageAddress
cout << "Value: " << *ageAddress << endl; //pointing to the ageAddress and printing what's at that address
cout << "Address2: " << &ageAddress << endl; //printing the address of the variable ageAddress

cout << "\n"; //cleating a blank line for clarity
system("pause"); //pausing the window to keep it open
}