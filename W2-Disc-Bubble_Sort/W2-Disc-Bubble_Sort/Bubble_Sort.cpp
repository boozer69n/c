//Student: Clinton Booze
//Professor: Victoria Heil
//Week 2 - Discussion - Bubble Sort

#include<iostream>
using namespace std;

int main()
{
	int shoeSize[] = { 5, 7, 1, 8, 4 };
	int numberOfElements = sizeof(shoeSize) / sizeof(int);

	for(int x = 0; x < numberOfElements; x++)
	{
		for(int y = 0; y < numberOfElements-1; y++)
		{
			if(shoeSize[y]>shoeSize[y+1])
			{
				int temp = shoeSize[y+1];
				shoeSize[y+1] = shoeSize[y];
				shoeSize[y] = temp;
			}
		}
	}

	for(int y = 0; y < numberOfElements; y++)
	{
		cout << shoeSize[y];
	}

	cout << "\n";
	system("pause");
}