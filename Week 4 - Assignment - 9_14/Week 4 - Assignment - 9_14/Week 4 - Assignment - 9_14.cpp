//Student: Clinton Booze
//Professor: Victoria Heil
//Week 4 - Assignment - 9_14
//############################################################################################################ 
#include <iostream> //for displaying
#include "HugeInteger.h"
using namespace std; //using std namespace
//############################################################################################################ 
/*
(HugeInteger Class) Create a class HugeInteger that uses 40-element array of digits to store integers as large
as 40 digits each. Provide member functions input, output, add, and subtract. For comparing HugeInteger objects,
provide functions isEqualTo, isNotEqualTo, isGreaterThan, isLessThan, isGreaterThanOrEqualTo and
isLessThanOrEqualTo - each of these is a "predicate" function that simply returns true if the relationship
holds between the two HugeIntegers and returns false if the relationship does not hold. Also provide a
predicate function isZero.  If you feel ambitious provide member functions multiply, divide, and modulus.
*/
//############################################################################################################ 
int main() //initialize program
{
	HugeInteger num1; //defining first number
	num1.input ("1928756890236458927364578");
	
	HugeInteger num2; //defining second number
	num2.input ("2983746598273648957263489");

	cout << "Adding Numbers\n" //adding the numbers
		 << "  " << num1.output() << endl
		 << "+ " << num2.output() << endl
		 << "---------------------------------------------\n";
	num1.add(num2);
	cout << "  " << num1.output() << endl << endl;

	cout << "Subtracting Numbers\n" //subtracting the numbers
		 << "  " << num1.output() << endl
		 << "- " << num2.output() << endl
		 << "---------------------------------------------\n";
	num1.subtract(num2);
	cout << "  " << num1.output() << endl << endl;

	cout << "Number Comparison\n"
		 << "Number 1 = " << num1.output() << endl
		 << "Number 2 = " << num2.output() << endl;

	//bool checks comparing numbers
	cout << "Number 1 == Number 2 - " << (num1.isEqualTo(num2) ? "True" : "False") << endl;
	cout << "Number 1 != Number 2 - " << (num1.isNotEqualTo(num2) ? "True" : "False") << endl;
	cout << "Number 1 >  Number 2 - " << (num1.isGreaterThan(num2) ? "True" : "False") << endl;
	cout << "Number 1 <  Number 2 - " << (num1.isLessThan(num2) ? "True" : "False") << endl;
	cout << "Number 1 >= Number 2 - " << (num1.isGreaterThanOrEqualTo(num2) ? "True" : "False") << endl;
	cout << "Number 1 <= Number 2 - " << (num1.isLessThanOrEqualTo(num2) ? "True" : "False") << endl;
	cout << "Number 1 =0 Number 2 - " << (num1.isZero() ? "True" : "False") << endl;
	system("pause"); //to keep window up
}