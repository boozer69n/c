#include "HugeInteger.h"
using std::stoi;
using std::to_string;

HugeInteger::HugeInteger() //constructor
{
	myArray.fill(0);
}

HugeInteger::~HugeInteger() //deconstructor
{
}

void HugeInteger::input (string firstNum) //input
{
	myArray.fill(0); //zeros out array
	int myArrayPos = 39; //track location
	for (int i = firstNum.length()-1; i >= 0; i--) //loop
	{
		myArray[myArrayPos] = stoi(firstNum.substr(i, 1)); //fill array
		myArrayPos--;
	}
}

string HugeInteger::output () //output
{
	string temp;
	bool foundFirstDigit = false; //exit check
	int tempPos = 0; //track position
	for (int i = 0; i < 40; i++) //loop
	{
		if (!foundFirstDigit && myArray[i] != 0) //looking for number
		{
			foundFirstDigit = true; //found
		}

		if (foundFirstDigit) //if found
		{
			temp.insert(tempPos, to_string(myArray[i])); //creating string
			tempPos++;
		}
	}
	return temp; //return string
}

void HugeInteger::add( const HugeInteger &number ) //add function
{
	char cNum1 = 0; //defining
	char cNum2 = 0; //defining
	char cCarry = 0; //defining

	for (int i = 39; i >= 0; i--) //loop
	{
		cNum1 =  this->myArray[i]; //getting number
		cNum2 =  number.myArray[i]; //getting number
		char result = char (cNum1+cNum2+cCarry); //adding
		if (result > 9) //checking for carry
		{
			cCarry = result / 10; //saving carry
			result = result % 10; //saving result
		}
		else
		{
			cCarry = 0;	//if no carry
		}

		this->myArray[i] = result; //filling array
	}
}

void HugeInteger::subtract( const HugeInteger &number ) //subtract function
{
	char cNum1 = 0; //defining
	char cNum2 = 0; //defining
	char cCarry = 0; //defining

	for (int i = 39; i >= 0; i--) //loop
	{
		cNum1 =  this->myArray[i]; //getting number
		cNum2 =  number.myArray[i]; //getting number
		char result = char (cNum1-cNum2+cCarry); //subtracting
		if (result < 0) //checking for carry
		{
			cCarry = -1; //setting carry
			result = result +10; //saving result
		}
		else
		{
			cCarry = 0; //resetting carry
		}

		this->myArray[i] = result; //saving array
	}
}

bool HugeInteger::isEqualTo( const HugeInteger &number ) //is equal function
{
	for (int i = 39; i >=0; i--)
	{
		if (this->myArray[i] != number.myArray[i])
		{
			return false;		
		}
	}
	return true;
}

bool HugeInteger::isNotEqualTo( const HugeInteger &number ) //is not equal function
{
	return !isEqualTo(number);
}

bool HugeInteger::isGreaterThan( const HugeInteger &number ) //is greater than function
{
	for (int i = 0; i <= 39; i++)
	{
		if (this->myArray[i] > number.myArray[i])
		{
			return true;
		}
		if (this->myArray[i] < number.myArray[i])
		{
			return false;
		}
	}
	return false;
}

bool HugeInteger::isLessThan( const HugeInteger &number ) //less than function
{
	return !isGreaterThanOrEqualTo(number);
}

bool HugeInteger::isGreaterThanOrEqualTo ( const HugeInteger &number ) //greater than or equal function
{
	return (isEqualTo(number) || isGreaterThan(number));
}

bool HugeInteger::isLessThanOrEqualTo( const HugeInteger &number ) //is less than or equal too function
{
	return (isEqualTo(number) || isLessThan(number));
}

bool HugeInteger::isZero () //checking if number is 0
{
	for (int i = 39; i >=0; i--)
	{
		if (this->myArray[i] != 0)
		{
			return false;		
		}
	}
	return true;
}