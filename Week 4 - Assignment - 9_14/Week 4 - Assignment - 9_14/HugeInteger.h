#include <array>
#include <string>
using std::array;
using std::string;

#ifndef HUGE_INTEGER_H //for compatibility
#define HUGE_INTEGER_H

class HugeInteger //creating class
{
private: //private
	array<int, 40> myArray; //40 element array

public: //public
	HugeInteger(); //constructor
	virtual ~HugeInteger(); //deconstructor

	void input (string); //input
	string output (); //output

	void add ( const HugeInteger & ); //add function
	void subtract ( const HugeInteger & ); //subtract function

	bool isEqualTo ( const HugeInteger & ); //equal check
	bool isNotEqualTo( const HugeInteger & ); //not equal check
	bool isGreaterThan( const HugeInteger & ); //greater than check
	bool isLessThan( const HugeInteger & ); //less than check
	bool isGreaterThanOrEqualTo ( const HugeInteger & ); //greater than or equal too check
	bool isLessThanOrEqualTo( const HugeInteger & ); //less than or equal too check
	bool isZero (); //is zero check
};
#endif  //end