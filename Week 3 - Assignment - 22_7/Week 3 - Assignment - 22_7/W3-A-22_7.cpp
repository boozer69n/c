/*
Student: Clinton Booze
Professor: Jill Coddington
Week 3 - Assignment - 22.7
//#################################################################################################
(Multiplication Via Bit Shifting)
Left-shifting an unsigned integer by one bit is equivalent to multiplying the value by 2. Write
function power2 that takes two integer arguments, number and pow, and calculates 
			number * 2^pow
Use a shift operator to calculate the result. The program should print the values as integers and as
bits.
//###############################################################################################*/

#include <iostream>
#include <iomanip>
#include <bitset>
using namespace std;

unsigned int power2 (unsigned int num, unsigned int pwr);
void bitDisplay (unsigned int number, unsigned int power, unsigned int answer);

//main
int main()
{
	//declaring variables
	unsigned int number;
	unsigned int pwr;
	unsigned int answer;

	//requesting input of numbers
	cout << "Equation: number * 2^power\nPlease fill in ""number"" & ""power""" << endl;
	cout << "Number: ";
	cin >> number;
	cout << "Power: ";
	cin >> pwr;

	//displaying numbers and answers
	answer = power2(number, pwr);
	cout << "Answer = " << answer << "\n" << endl;
	cout << right << setw(8) << number;
	cout << " * 2^ ";
	cout << right << setw(8) << pwr;
	cout << " = ";
	cout << right << setw(16) << answer << endl;
	bitDisplay(number, pwr, answer);
	system("pause");
}

//multiply function
unsigned int power2 (unsigned int num, unsigned int pwr)
{
	unsigned int answer;
	//shifting answer
	answer = num << pwr;
	
	return answer;
}

//displaying numbers in byte form
void bitDisplay (unsigned int number, unsigned int power, unsigned int answer)
{
	cout << (bitset<8>(number)) << " * 2^ " << (bitset<8>(power)) << " = " << (bitset<16>(answer)) << endl;
}