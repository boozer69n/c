// Fig. 22.14: DeckOfCards.h
#include <array>

// BitCard structure definition with bit fields
struct BitCard
{
	unsigned face : 4; // 4 bits; 0-15
	unsigned suit : 2; // 2 bits; 0-3
	unsigned color : 1; // 1 bit; 0-1
}; // end struct BitCard

// DeckOfCards class definition
class DeckOfCards
{
public:
	static const int faces = 13; // 2 thru Ace
	static const int colors = 2; // black and red
	static const int numberOfCards = 52; //52 cards in a deck of cards

	DeckOfCards(); // constructor initializes deck
	void deal(); // deals cards in deck
private:
	/*An array of the structure BitCard which contains the face of the card,
	suit of the card, and its color. The second argument of numberOfCards is
	52 as stated above, meaning the array has 52 elements of the structure
	BitCard and the object to represent them is deck*/
	std::array< BitCard, numberOfCards > deck;
}; // end class DeckOfCards