// Fig. 22.15: DeckOfCards.cpp
#include <iostream>
#include <iomanip>
#include "DeckOfCards.h" // DeckOfCards class definition
using namespace std;

// no-argument DeckOfCards constructor intializes deck
DeckOfCards::DeckOfCards()                                 
{
	/*a for loop to cycle through the array of BitCard
	structures to fill each variable within the structure
	a face value, suit value, and a color value*/
	for ( size_t i = 0; i < deck.size(); ++i ) 
	{
		//assigning the faces 0-12 (13-total)
		deck[ i ].face = i % faces;
		//assigning the suit 0-3 (4-total)
		deck[ i ].suit = i / faces;
		//assigning the color 0-1 (2-total)
		deck[ i ].color = i / ( faces * colors );
	} // end for
} // end no-argument DeckOfCards constructor

//function to simulate dealing all the cards in the deck
void DeckOfCards::deal()
{
	//for loop for displaying the deck of cards
	for ( size_t k1 = 0, k2 = k1 + deck.size() / 2; 
		k1 < deck.size() / 2 - 1; ++k1, ++k2 ) 
			//displaying the first half of cards
			cout << "Card:" << setw( 3 ) << deck[ k1 ].face 
			<< "  Suit:" << setw( 2 ) << deck[ k1 ].suit 
			<< "  Color:" << setw( 2 ) << deck[ k1 ].color

			//displaying the second half of cards
			<< "   " << "Card:" << setw( 3 ) << deck[ k2 ].face
			<< "  Suit:" << setw( 2 ) << deck[ k2 ].suit 
			<< "  Color:" << setw( 2 ) << deck[ k2 ].color << endl;
} // end function deal