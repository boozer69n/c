// Fig. 22.16: fig22_16.cpp
#include "DeckOfCards.h" // DeckOfCards class definition

//begin main
int main()
{
   DeckOfCards deckOfCards; // create DeckOfCards object
   deckOfCards.deal(); // deal the cards in the deck
   system("pause"); //added to keep window up
} // end main