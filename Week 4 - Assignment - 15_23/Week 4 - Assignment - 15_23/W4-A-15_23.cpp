/*
Student: Clinton Booze
Professor: Jill Coddington
Week 4 - Assignment - 15.23
#################################################################################################
(Palindromes)
Write a function template palindrome that takes a vector parameter and returns
true or false according to whether the vector does or does not read the same forward as backward
(e.g., a vector containing 1, 2, 3, 2, 1 is a palindrome, but a vector containing 1, 2, 3, 4 is
not).
#################################################################################################*/

#include <iostream>
#include <array>
#include <algorithm>
#include <vector>
using namespace std;

//declaring
template<class T> bool palindrome(T &myVector);

int main()
{
	//creating an array and populating the vector with the array info
	//numbers formatted to return true
	array<int, 5> myArrayTrue = {1,2,3,2,1};
	vector<int> myVectorTrue(myArrayTrue.cbegin(), myArrayTrue.cend());
	
	//creating an array and populating the vector with the array info
	//numbers formatted to return false
	array<int, 5> myArrayFalse = {1,2,3,4,5};
	vector<int> myVectorFalse(myArrayFalse.cbegin(), myArrayFalse.cend());

	//creating an array and populating the vector with the array info
	//numbers formatted to return true
	array<char, 5> myCharArrayTrue = {'a','b','c','b','a'};
	vector<char> myCharVectorTrue(myCharArrayTrue.cbegin(), myCharArrayTrue.cend());

	//creating an array and populating the vector with the array info
	//numbers formatted to return false
	array<char, 5> myCharArrayFalse = {'a','b','c','d','e'};
	vector<char> myCharVectorFalse(myCharArrayFalse.cbegin(), myCharArrayFalse.cend());
	

	//displaying true or false depending on array/vector numbers
	cout << boolalpha << "Should be true: " << palindrome(myVectorTrue) << endl;
	cout << boolalpha <<  "Should be false: " << palindrome(myVectorFalse) << endl;
	cout << boolalpha <<  "Should be true: " << palindrome(myCharVectorTrue) << endl;
	cout << boolalpha <<  "Should be false: " << palindrome(myCharVectorFalse) << endl;
	system("pause");
}

//template that will run through any vector to check if it is a palindrome
template<class T> bool palindrome(T &myVector)
{
	return equal(myVector.begin(),myVector.end(),myVector.rbegin());
}