//Student: Clinton Booze
//Professor: Victoria Heil
//Week 4 - Assignment - 10_10
//############################################################################################################ 
#include <iostream> //for displaying
#include "RationalNumber.h" //including header for class functions
using namespace std;
//############################################################################################################ 
/*
(RationalNumber Class) Create a class RationalNumber (fractions) with these capabilities:
a - Create a constructor that prevents a 0 denominator in a fraction, reduces or simplifies fractions that
    are not in reduced form and avoids negative denominators.
b - Overload the addition, subtraction, multiplication and division operators for this class.
c - Overload the relation and equality operators for this class.
*/
//############################################################################################################ 
int main()
{
	RationalNumber num1(6,8), num2(3,7), result;

	result = num1 + num2; //addition
	cout << num1 << " + " << num2 << " = " << result << endl;

	result = num1 - num2; //subtraction
	cout << num1 << " - " << num2 << " = " << result << endl;

	result = num1 * num2; //multiply
	cout << num1 << " * " << num2 << " = " << result << endl;

	result = num1 / num2; //division
	cout << num1 << " / " << num2 << " = " << result << endl;

	//bool function check
	cout << endl;
	cout << num1 << " < " << num2 << " = " << (num1 < num2 ? "True" : "False") << endl;
	cout << num1 << " > " << num2 << " = " << (num1 > num2 ? "True" : "False") << endl;
	cout << num1 << " <= " << num2 << " = " << (num1 <= num2 ? "True" : "False") << endl;
	cout << num1 << " >= " << num2 << " = " << (num1 >= num2 ? "True" : "False") << endl;
	cout << num1 << " != " << num2 << " = " << (num1 != num2 ? "True" : "False") << endl;
	cout << num1 << " == " << num2 << " = " << (num1 == num2 ? "True" : "False") << endl;
	  
	system("PAUSE"); //to keep window open
	return 0;
}