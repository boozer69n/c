#include "RationalNumber.h"
using namespace std;

RationalNumber::RationalNumber(int num, int den) //constructor
{
	numerator = num; //defining numerator
	if (den <= 0) //checking if denominator is <= 0
	{
		den = 1; //if it is then set to 1
	}
	denomerator = den; //defining denominator
	simplify(); //simplifying numbers
}

RationalNumber::~RationalNumber(void) //destructor
{
}

RationalNumber RationalNumber::operator + ( const RationalNumber &rNum ) //defining +
{
	int comDen = (this->denomerator * rNum.denomerator);
	int num1 = (rNum.denomerator * this->numerator);
	int num2 = (this->denomerator * rNum.numerator);

	return RationalNumber((num1+num2), comDen);
}

RationalNumber RationalNumber::operator - ( const RationalNumber &rNum ) //defining -
{
	int comDen = (this->denomerator * rNum.denomerator);
	int num1 = (rNum.denomerator * this->numerator);
	int num2 = (this->denomerator * rNum.numerator);

	return RationalNumber((num1-num2), comDen);
}

RationalNumber RationalNumber::operator * ( const RationalNumber &rNum ) //defining *
{
	int num = (this->numerator * rNum.numerator);
	int den = (this->denomerator * rNum.denomerator);

	return RationalNumber(num, den);
}

RationalNumber RationalNumber::operator / ( const RationalNumber &rNum ) //defining /
{
	int den = (this->denomerator * rNum.numerator);
	int num = (this->numerator * rNum.denomerator);

	return RationalNumber(num, den);
}

RationalNumber RationalNumber::operator = ( const RationalNumber &rNum ) //defining =
{
	this->numerator = rNum.numerator;
	this->denomerator = rNum.denomerator;
	return *this;
}

bool RationalNumber::operator == ( const RationalNumber &rNum ) //defining ==
{
	return (this->numerator == rNum.numerator && this->denomerator == rNum.denomerator);
}

bool RationalNumber::operator != ( const RationalNumber &rNum ) //defining !=
{
	return (!(*this == rNum));
}

bool RationalNumber::operator < ( const RationalNumber &rNum ) //defining <
{
	return ((rNum.denomerator * this->numerator) < (this->denomerator * rNum.numerator));
}

bool RationalNumber::operator <= ( const RationalNumber &rNum ) //defining <=
{
	return ((*this == rNum) || (*this < rNum));
}

bool RationalNumber::operator > ( const RationalNumber &rNum ) //defining >
{
	return (!(*this <= rNum));
}

bool RationalNumber::operator >= ( const RationalNumber &rNum ) //defining >=
{
	return ((*this == rNum) || (*this > rNum));
}

ostream & operator << ( ostream& stream, const RationalNumber &rNum ) //defining <<
{
	stream << rNum.numerator << "/" << rNum.denomerator;
	return stream;
}

void RationalNumber::simplify () //defining the simplify function
{
	if (denomerator == 1)
	{
		return;
	}
	int gcd = 1;
	int small = (numerator < denomerator) ? numerator : denomerator; //finding which is smaller, numerator/denominator
	for (int i = 2; i <= small; i++) //loop going to lowest number starting at 2
	{
		if (numerator % i == 0 && denomerator % i == 0) //seeing if the number worked as a gcd
		{
			gcd = i;
		}
	}
	numerator /= gcd; //sets new numerator
	denomerator /= gcd; //sets new denominator
}