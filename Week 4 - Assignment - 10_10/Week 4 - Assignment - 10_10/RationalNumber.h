#include <ostream>
#include <istream>
using namespace std;
#ifndef RATIONAL_NUMBER_H //for compatibility
#define RATIONAL_NUMBER_H

class RationalNumber //creating class info
{
friend ostream &operator << ( ostream& , const RationalNumber &rNum ); //added to display numbers

public: //public functions
    RationalNumber( int num = 1, int den = 1 ); //constructor
	virtual ~RationalNumber(); //destructor

    RationalNumber operator + ( const RationalNumber &rNum ); //operator +
    RationalNumber operator - ( const RationalNumber &rNum ); //operator -
    RationalNumber operator * ( const RationalNumber &rNum ); //operator *
    RationalNumber operator / ( const RationalNumber &rNum ); //operator /
    RationalNumber operator = ( const RationalNumber &rNum ); //operator =
    bool operator == ( const RationalNumber &rNum ); //operator ==
    bool operator != ( const RationalNumber &rNum ); //operator !=
    bool operator < ( const RationalNumber &rNum ); //operator <
    bool operator <= ( const RationalNumber &rNum ); //operator <=
    bool operator > ( const RationalNumber &rNum ); //operator >
    bool operator >= ( const RationalNumber &rNum ); //operator >=

private:
	int numerator; //defining numerator
	int denomerator; //defining denominator
	void simplify (); //defining simplify
};
#endif //end