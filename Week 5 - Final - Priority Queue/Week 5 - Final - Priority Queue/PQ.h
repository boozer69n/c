#ifndef PQ_H
#define PQ_H

#include <string>
#include <list>
#include <time.h>

using namespace std;

class PQ {

public:
struct node {
	string first_name;
	string last_name;
	int base_priority;
	int priority;
	clock_t create_time;
	bool operator < (const node& right) const { return priority < right.priority; }
};

private:
	list<node> answer_list;
	node create_node(string first, string last, int priority);
	void update_priority();
	
public:
	PQ();
	virtual ~PQ();
	void push(string first, string last, int priority);
	void push(string first, string last);
	node pop();
	void display_caller(node caller);
	void display_queue();
	void empty_pq();
};

#endif