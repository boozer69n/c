#include <time.h>
#include <iostream>
#include <string>
#include <list>
#include <stdio.h>
#include "PQ.h"
using namespace std;

PQ::PQ(void){}

//creating a node and returning it
PQ::node PQ::create_node(string first, string last, int priority){
	node* new_node = new node;
	new_node->first_name = first;
	new_node->last_name = last;
	new_node->base_priority = priority;
	new_node->priority = priority;
	new_node->create_time = clock();
	return *new_node;
}

//addign a new caller to the queue
void PQ::push(string first, string last, int priority){
	this->answer_list.push_front(create_node(first, last, priority));
}

//overloaded to have a default priority set
void PQ::push(string first, string last){
	this->answer_list.push_front(create_node(first, last, 1));
}

//update priority based off of time
void PQ::update_priority(){
	clock_t time = clock();
	for (list<node>::iterator it = answer_list.begin(); it != answer_list.end(); it++){
		it->priority = it->base_priority + ((time - it->create_time)/300000);
	}
	answer_list.sort();
}

//pop off a node from the back and return it
PQ::node PQ::pop(){
	update_priority();
	node temp = answer_list.back();
	answer_list.pop_back();
	return temp;
}

//display a caller and their information
void PQ::display_caller(node caller){
	int hold_time = (((clock() - caller.create_time)/1000)/60);
	cout << "\n\nName: " << caller.first_name << " " << caller.last_name << endl;
	cout << "Starting Priority: " << caller.base_priority << endl;
	cout << "Current Priority: " << caller.priority << endl;
	cout << "Time on Hold: " << hold_time << endl;
}

//display everyone on hold
void PQ::display_queue(){
	update_priority();
	clock_t time = clock();
	int num = 1;
	printf("\nCurrent Queue - Size: %i\n", answer_list.size());
	for (list<node>::iterator it = answer_list.begin(); it != answer_list.end(); ++it){
		cout << "--------------------------------------------------" << endl;
		cout << num << " - " << it->first_name << " " << it->last_name << endl;
		cout << "Priority Rating: " << it->priority << endl;
		cout << "Minutes on Hold: " << ((clock() - it->create_time)/1000)/60 << endl;
		num++;
	}
}

//empty the list
void PQ::empty_pq(){
	answer_list.clear();
}

//destructor
PQ::~PQ(void){ empty_pq(); }