/*
Final Project Proposal
Review the requirements of the Final Project. Post a brief description of the data structure
you will use and the real-world application you will be embedding the data structure for the
Final Project in. Describe why you chose the data structure and the application, and list the
challenges you think you may encounter. Think carefully about the scope and time frame of the
project.

###################################################################################################

I would like to use a Priority Queue and here is how I would like to implement it:

I would like to have it be somewhat of a program that runs in the background of a phone service
place, maybe something like a place that does technical support.
So someone calls in and the lets say receptionist or just an automated thing will assign a
priority level to the call then putting it in the queue, the queue will place it in the
appropriate location within the queue according to priority.
So when the technician accepts the next call the persons information will show on their screen
and their technical issue to service them better.
I would like to use a Linked List to back it.

Something along those lines, what I would like to do is also implement a timing to the people
already on hold, so for example every 5 minutes you priority level gets pushed up a bit to help
the tech's get to your call. For testing purposes and to speed things up I will most likely make
it something like 20 sec so the professor doesn't have to wait the 5 min...lol

-Clinton
###################################################################################################
*/

#include <iostream>
#include "PQ.h"
#include <ctype.h>
#include <stdio.h>
#include <string>

PQ calls;

void fill_queue();
void run_ui();
void tech();
void input();

int main(){
	fill_queue();
	run_ui();
}

//the ui for the options
void run_ui(){
	bool should_exit = false;
	char choice;

	while(!should_exit){
		cout << "\nPlease Choose" << endl;
		cout << "1 - Answer Calls" << endl;
		cout << "2 - Input Callers" << endl;
		cout << "3 - View Callers on Hold" << endl;
		cout << "0 - End" << endl;
		cout << "Choice: ";
		cin >> choice;

		switch (tolower(choice)){
		case '1':
			tech();
			break;
		case '2':
			input();
			break;
		case '3':
			calls.display_queue();
			break;
		default:
			should_exit = true;
			break;
		}
	}
}

//used for an example of answering a phone call
void tech(){
	char choice;
	bool should_exit = false;

	while (!should_exit){
		cout << "\nReady for the next call? (y/n) ";
		cin >> choice;
		switch (tolower(choice)){
		case 'y':
			calls.display_caller(calls.pop());
			break;
		default:
			should_exit = true;
			break;
		}
	}
}

//used for an example of inputting information
void input(){
	char choice, new_caller;
	string first, last;
	int priority;
	bool should_exit = false;

	while(!should_exit){
		cout << "\nAdd a new caller? (y/n) ";
		cin >> new_caller;
		switch (tolower(new_caller)){
		case 'y':
			cout << "\nCustomers Information" << endl;
			cout << "First Name: ";
			cin >> first;
			cout << "\nLast Name: ";
			cin >> last;
			cout << "Set Priority (y/n)? ";
			cin >> choice;

			switch (tolower(choice)){
			case 'y':
				cout << "Priority (1-10): ";
				cin >> priority;
				calls.push(first,last,priority);
				break;
			default:
				calls.push(first,last);
				break;
			}
			break;
		default:
			should_exit = true;
			break;
		}
	}
}

//adds some random people to the call list
void fill_queue(){
	calls.push("Clinton","Booze",5);
	calls.push("Caleb","Jacobs",7);
	calls.push("Elisha","Mcnutt");
	calls.push("Ian","Tyler",2);
	calls.push("Adrian","Cameron",6);
	calls.push("James ","Rogers",3);
	calls.push("Justin ","Strasburg");
	calls.push("Emma ","Magrane");
	calls.push("Matthew ","Wolford");
}