#include "Complex.h" //including header
#include <iostream> //including input/output
using namespace std; //std namespace

Complex::Complex(void) //constructor
{
	realNum = 0; //setting default
	imaginaryNum = 0; //setting default
	sign = '+'; //setting default
}

//overloading output to display in order and return
ostream& operator << (ostream &newOut, const Complex &numObj)
{ //begin overload
	newOut << numObj.realNum //real number
		   << " " //space
		   << numObj.sign //sign
		   << " " //space
		   << numObj.imaginaryNum //imaginary number
		   << "i" << endl; //imaginary sign

	return newOut; //returns object
} //end overload

//overloading input, saves, returns a fail if input wrong
istream& operator >> (istream &newIn, Complex &numObj)
{ //begin overload
	newIn >> numObj.realNum; //saves real number
	newIn.ignore(1); //ignores 1 char
	newIn >> numObj.sign; //saves sign
	newIn.ignore(1); //ignores 1 char
	newIn >> numObj.imaginaryNum; //saves imaginary number

	return newIn; //returns object
} //end overload

void Complex::setReal (int &real) //function to set real
{ //begin function
	this->realNum = real; //sets real
} //end function

void Complex::setImaginary (int &imaginary) //function to set imaginary
{ //begin function
	this->imaginaryNum = imaginary; //sets imaginary
} //end function

void Complex::setSign () //function to update sign
{ //begin function
	if(this->imaginaryNum >= 0) //checking if imaginary is positive
	{ //begin if
		this->sign = '+'; //sets sign to positive
	} //end if

	if(this->imaginaryNum < 0) //checking if imaginary number is negative
	{ //begin if
		this->sign = '-'; //sets sign to negative
		//converts imaginary number to positive for correct display
		this->imaginaryNum = abs(this->imaginaryNum); 
	} //end if
} //end function

Complex::~Complex(void) //destructor
{
}