#ifndef COMPLEX_H //for compatibility
#define COMPLEX_H //for compatibility

#include <iostream> //for input/output
#include <string> //for string

using namespace std; //std namespace

class Complex //creating class
{ //begin class
public: //publicly available
	Complex(void);
	friend ostream& operator << (ostream &, const Complex &); //including ostream to overload
	friend istream& operator >> (istream &, Complex &); //including istream to overload
	
	void setReal (int &real); //setter
	void setImaginary (int &imaginary); //setter
	void setSign (); //setter

	~Complex(void);

private: //private variables
	int realNum; //real number
	int imaginaryNum; //imaginary number
	char sign; //sign
}; //end class

#endif