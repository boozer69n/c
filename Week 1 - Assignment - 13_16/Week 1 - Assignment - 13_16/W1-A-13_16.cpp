/*
Student: Clinton Booze
Professor: Jill Coddington
Week 1 - Assignment - 13.16
//#################################################################################################
Write a program that accomplishes each of the following:
A - Create A user-defined class "Complex" that contains the private integer data members "real" and
	"imaginary" and declares stream insertion and stream extraction overloaded operator functions
	as "friends" of the class.
B - Define the stream insertion and stream extraction operator functions. The stream extraction
	operator function should determine whether the data entered is valid, and, if not, it should
	"set failbit" to indicate improper input. The input should be of the form
											"3 + 8i"
C - The values can be negative or positive, and it's possible that one of the two values is not
	provided, in which case the appropriate data member should be set to 0. The stream insertion
	operator should not be able to display the point if an input error occurred. For negative
	imaginary values, a minus sign should be printed rather than a plus sign.
D - Write a "main" function that tests input and output of user-defined class "Complex", using
	the overloaded stream extraction and stream insertion operators.
	*/
//#################################################################################################
#include <iostream> //for input/output
#include "Complex.h" //including class
using namespace std; //std namespace

Complex numEq; //creating equation object

void numCheckSet (int number, string type, Complex &cplx); //declaring function
//#################################################################################################
int main() //main
{ //begin main
	string input; //string for user input
	do //while loop
	{ //begin loop
		//displaying current equation
		cout << "\nYour current equation is: " << numEq << endl;
		//UI
		cout << "Please choose one" << endl <<
				"1 - Change REAL number" << endl <<
				"2 - Change IMAGINARY number" << endl <<
				"3 - Enter whole equation" << endl <<
				"0 - EXIT" << endl <<
				"Selection: ";
		cin >> input; //saving user selection

		if (input == "1") //real number modification
		{ //begin if
			int userReal; //int for new real number
			cout << "Real Number: "; //displaying request for number
			cin >> userReal; //saving number
			numCheckSet (userReal, "real", numEq); //sending to function
			numEq.setSign(); //updating sign if necessary
		} //end if
		if (input == "2") //imaginary number modification
		{ //begin if
			int userImaginary; //int for imaginary number
			cout << "Imaginary Number: "; //displaying request for number
			cin >> userImaginary; //saving number
			numCheckSet (userImaginary, "imaginary", numEq); //sending to function
			numEq.setSign(); //updating sign if necessary
		} //end if
		if (input == "3") //whole equation modification
		{ //begin if
			cout << "Equation: "; //displaying request for equation
			cin >> numEq; //inputting equation through overloaded operator

			if(cin.fail()) //checks for valid input
			{ //begin if
				cout << "Invalid Input" << endl; //message if invalid input
			} //end if
			cin.clear(); //clearing excess
			cin.ignore(256,'\n'); //clearing excess
		} //end if
		//checks for invalid input
		if (input != "0" && input != "1" && input != "2" && input != "3") 
		{ //begin if
			cout << "Invalid Input" << endl; //message if invalid input
		} //end if
	} while (input != "0" & !cin.fail()); //end loop
} //end main
//#################################################################################################
//function to check if the number entered is valid and to set it if it is
void numCheckSet (int number, string type, Complex &cplx)
{ //begin function
	if(!cin.fail() && (cin.peek()==EOF || cin.peek()=='\n')) //checking if number is valid
	{ //begin if
		cin.clear(); //clearing excess
		cin.ignore(256,'\n'); //clearing excess
		if(type == "real") //if the number to be changes is the real number
		{ //begin if
			cplx.setReal(number); //changing the real number in the object
		} //end if
		else //else
		{ //begin else
			cplx.setImaginary(number); //changing the imaginary number
		} //end else
	} //end if
	else //else
	{ //begin else
		cin.clear(); //clearing excess
		cin.ignore(256,'\n'); //clearing excess
		cout << "Invalid Input" << endl; //invalid input message
	} //end else
} //end function
