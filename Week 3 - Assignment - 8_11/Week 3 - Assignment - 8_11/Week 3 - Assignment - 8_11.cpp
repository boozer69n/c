/*
 * AUTHOR: CLINTON BOOZE
 * CLASS: C/C++ PROGRAMMING I
 * INSTRUCTOR: VICTORIA HEIL
 * DESCRIPTION: WEEK 3 - ASSIGNMENT 8.11
 */

#include <iostream> // to display to the user
using namespace std; //to using std namespace

void QuestionA() //defining function for problem a
{
	/******** Problem A - ORIGINAL *******
	int *number;
	cout << number << endl;
	*/

	/******** WHAT'S WRONG *******
	problem was that number was defined as a pointer
	but was not actually pointing at anything
	*/

	//******** Problem A - REVISED *******
	cout << "Question A:\n";

	int *number; //defining number as a pointer
	int testVar = 5; //created a test variable
	number = &testVar; //pointed to the test variable with the number pointer
	cout << number << endl; //displaying number which is the test variable memory location

	cout << endl; //space for clarity
}

void QuestionB() //defining function for problem b
{
	/*********** Problem B - ORIGINAL **********
	double *realPtr;
	long *integerPtr;
	integerPtr = realPtr;
	*/

	/******** WHAT'S WRONG *******
	cant have different types for pointers pointing at each other without converting it
	also needed a variable to point at
	*/

	//*********** Problem B - REVISED **********
	cout << "Question B:\n";

	double *realPtr; //declaring pointer double realPtr
	long *integerPtr; //declaring pointer long integerPtr
	double testVar; //declaring variable testVar
	realPtr = &testVar; //pointing realPtr at testVar
	integerPtr = (long*)realPtr; //changing double realPtr to long realPtr
	
	cout << integerPtr << endl; //displays integerPtr
	cout << realPtr << endl; //displays realPtr

	cout << endl; //space for clarity
}

void QuestionC() //defining function for problem c
{
	/*********** Problem C - ORIGINAL **********
	int * x, y;
	x = y;
	*/

	/******** WHAT'S WRONG *******
	y was missing the & to allow the pointer x to get the address of y
	*/

	//*********** Problem C - REVISED **********
	cout << "Question C:\n";

	int * x, y; //defining x as an int pointer and y as an int variable
	x = &y; //pointing at the memory location of y and assigning it to x

	cout << x << endl; //displaying variable x; the memory location of y
	cout << &y << endl; //displaying the memory location of y

	cout << endl; //space for clarity
}

void QuestionD() //defining function for problem d
{
	/*********** Problem D - ORIGINAL **********
	char s[] = "this is a character array";
	for ( ; *s != '\0'; ++s)
		cout << *s << ' ';
	*/

	/******** WHAT'S WRONG *******
	something to point at the array
	the pointer inserted into the for loop instead of the array to step through the array
	*/

	//*********** Problem D - REVISED **********
	cout << "Question D:\n";

	char s[] = "this is a character array"; //creating an array with a null character at the end (built-in)
	char *sPtr = s; //created sPtr to point at array s
	for ( ; *sPtr != '\0'; ++sPtr) //a loop to run through the array until it finds the built in null character
		cout << *sPtr << ' '; //displays each character in the array with a space between

	cout << endl << endl; //space for clarity
}

void QuestionE() //defining function for problem e
{
	/*********** Problem E - ORIGINAL **********
	short *numPtr, result;
	void *genericPtr = numPtr;
	result = *genericPtr + 7;
	*/

	/******** WHAT'S WRONG *******
	numPtr needed something to point at
	generic needed to be converted to a short	
	*/

	// REVISED
	
	cout << "Question E:\n";

	short *numPtr, result; //creates a short pointer and a short variable
	short testVar = 5; //creates a short test variable
	numPtr = &testVar; //points numPtr at the memory address of test variable
	void *genericPtr = numPtr; //void genericPtr points at short numPtr
	result = *((short*)genericPtr) + 7; //result equals genericPtr
										//which is pointing at numPtr
										//which is pointing at testVar
										//which equals 5, so adding 7 = 12
	cout << result; //displays result
	cout << endl << endl; //space for clarity
}

void QuestionF() //defining function for problem f
{
	/*********** Problem F - ORIGINAL **********
	double x = 19.34;
	double xPtr = &x;
	cout << xPtr << endl;
	*/

	/******** WHAT'S WRONG *******
	xPtr was missing the * to actually point at the x address
	*/

	//*********** Problem F - REVISED **********
	cout << "Question F:\n";
	double x = 19.34; //creating the double variable x
	double *xPtr = &x; //xPtr pointing at the memory location of x
	cout << xPtr << endl; //displaying the variable xPtr which is the memory location of x
}

int main() //program initializer
{
	QuestionA();
	QuestionB();
	QuestionC();
	QuestionD();
	QuestionE();
	QuestionF();

	system("pause"); //keeps window up
}